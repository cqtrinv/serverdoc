#      Building production Server container
#
FROM nginx:1.27.4
LABEL maintainer="ChristianQueinnec <gerant@trinv.fr>"

ENV HOSTNAME=doc.trinv.fr
EXPOSE 80

RUN export DEBIAN_FRONTEND="noninteractive" && \
    cd /root/ && \
    apt-get update && \
    apt-get install -y --no-install-recommends dumb-init curl && \
    rm -rf /var/lib/apt/lists/*

COPY ./nginx/conf.d/default.conf /etc/nginx/conf.d/
# m generate.certificate
COPY ./nginx/cert/doctrinvkey.pem  /etc/ssl/private/
COPY ./nginx/cert/doctrinvcert.pem /etc/ssl/certs/
RUN chown nginx: /etc/ssl/*/doctrinv*pem

ENTRYPOINT [ "/usr/bin/dumb-init", "--" ]
CMD ["nginx", "-g", "daemon off;"]

RUN mkdir -p /usr/share/nginx/html
COPY buildlocal /usr/share/nginx/html/

# end.
