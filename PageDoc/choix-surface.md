---
layout: doclayout.liquid
pageTitle: Choix de la surface
tags: [ 'ContextualDoc' ]
---

Vous pouvez également indiquer un intervalle de surfaces
sous la forme <code>min-max</code> (par exemple <code>499-502</code>)
mais pensez à ne pas en demander trop car le nombre de parcelles 
résultant d'une requête est limité à 50.

Si les données de la commune ne sont pas encore présentes, il
vous faudra d'abord attendre quelques minutes avant que les
réponses apparaissent. Une notification vous avertira.

Il vous est déjà possible d'interagir avec la carte ci-dessous.
Cliquer sur un point affiche la parcelle concernée, sa surface, 
ses coordonnées et procure des liens vers le cadastre ou les
photos aériennes. Pratique lorsque vous connaissez la localisation
mais pas l'adresse!

La carte peut s'afficher en plein écran avec le bouton en haut à
droite. On peut passer à la vue aérienne avec le bouton M à gauche
mais en cliquant à nouveau sur ce bouton, on obtient les parcelles
cadastrales lorsque l'on est au bon niveau de zoom.

## DPE

Un bouton vous permet d'afficher les DPE relatifs à la commune
concernée. Les DPE étant obligatoires pour la mise en vente d'un bien,
leur connaissance permet souvent de trouver des adresses de
biens à vendre. Il y a cependant quelques
[inconvénients](/a_propos_des_dpes) à leur usage.

<style>
code {
 color: #910e3a;
}
</style>
