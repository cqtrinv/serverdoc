// Eleventy configuration

/* 
   
*/

//import { minify } from 'html-minifier';

export default function(eleventyConfig) {
    // API is available in `eleventyConfig` argument
    //console.log('loaded', eleventyConfig);

    /*
    eleventyConfig.addTransform("htmlmin", function(content, outputPath) {
        if ( outputPath.endsWith(".html") ) {
            let minified = minify(content, {
                useShortDoctype: true,
                removeComments: true,
                collapseWhitespace: true
            });
            return minified;
        }
        return content;
    });
    */
    
    return {
        // your normal config options
        markdownTemplateEngine: "liquid"
    };
};
