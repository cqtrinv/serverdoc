---
layout: doclayout.liquid
pageTitle: Choix de la commune
tags: [ 'ContextualDoc' ]
---

Tapez au moins 3 lettres pour déclencher la complétion automatique
puis choisissez lorsqu'il ne reste plus que quelques possibilités.

Les communes ont souvent plusieurs graphies, toutes ne sont pas
nécessairement connues de TRINV.

Essayez une graphie avec ou sans tiret, `CONQUES-SUR-ORBIEL` et
`CONQUES SUR ORBIEL` existent mais ni `CONQUES-EN-ROUERGUE`, 
ni `THIN LE MOUTIER`.

Supprimez ou ajoutez éventuellement le préfixe `LE` ou `LA`. Si 
`LE CONQUET` existe, `CONQUET` est plus facile à taper. 
En revanche, `LA HAGUE` n'est connue que comme `HAGUE`.

Tous les ans, une centaine de communes disparaissent.
Il y a des fusions de communes, des communautés de
communes, des intercommunalités, des communes déléguées, etc.

Quelques exemples:

- BINIC a fusionné avec ETABLES-SUR-MER pour donner BINIC-ETABLES-SUR-MER,
- ILE-MOLENE est maintenant rattachée à LE CONQUET qui n'a pas changé de nom.
- SAINT AMANT DE BONNIEURE, SAINT-ANGEAU et quelques autres sont devenus VAL DE BONNIEURE qui n'existait pas auparavant.

Si vous cherchez une commune disparue, il vous faut chercher dans la
commune englobante, le plus souvent une commune proche. Pour ce faire,
je consulte en général <a href='https://fr.wikipedia.org/'>
Wikipédia</a> qui indique souvent quand
la commune a disparu et ce qu'elle est devenue.

