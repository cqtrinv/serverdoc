---
layout: doclayout.liquid
pageTitle: Carte des parcelles
tags: [ 'ContextualDoc' ]
---

La liste des parcelles s'affiche ainsi que la carte géographique associée.

Cliquer sur un item des listes affiche la parcelle concernée. Vous
pouvez également cliquer sur une des marques disposées sur la carte.
Dans les deux cas, l'affichage permet d'accéder au cadastre et aux
vues aériennes. Cliquer sur NON, retire la parcelle de la liste.

Il y a des parcelles sans adresse! C'est notamment le cas de champs
éloignés de toute habitation. TRINV ignore les adresses qui sont à
plus de 100 mètres de la parcelle considérée et n'affiche que
l'identifiant de la parcelle cadastrale.

Cliquez sur "Parcelle' ou 'm²' pour trier le tableau suivant la
colonne.

Il est aussi possible d'interagir avec la carte ci-dessous.
Cliquer sur un point affiche la parcelle concernée, sa surface, 
ses coordonnées et procure des liens vers le cadastre ou les
photos aériennes. Pratique lorsque vous connaissez la localisation
mais pas l'adresse!

La carte peut s'afficher en plein écran avec le bouton en haut à
droite. On peut passer à la vue aérienne avec le bouton M à gauche
mais en cliquant à nouveau sur ce bouton, on obtient les parcelles
cadastrales lorsque l'on est au bon niveau de zoom.

On peut également zoomer puis restreindre la liste des parcelles à
considérer (avec le bouton à gauche en forme de carré) en éliminant de
la liste les parcelles qui ne sont plus visibles.
