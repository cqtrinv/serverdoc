---
layout: doclayout.liquid
pageTitle: Carte des favoris
tags: [ 'ContextualDoc' ]
---

Cette carte affiche toutes vos fiches mais seulement celles qui sont
associées à des adresses ou des coordonnées géographiques.

Vous pouvez cliquer sur les marqueurs présents sur la carte ou sur les
vignettes des annonces sous la carte et ainsi accéder à leur fiche.

Si vous souhaitez aussi afficher les DPE, vous devez revenir à
l'accueil, choisir une commune puis demander les DPE associés et, 
enfin, sur la carte résultante, faire apparaître vos fiches de
la commune concernée.






