---
layout: doclayout.liquid
pageTitle: Fiches favorites
tags: [ 'ContextualDoc' ]
---

Attention, la disposition du tableau dépend de la largeur d'écran.

Cliquer sur les titres des colonnes permet, suivant la nature des
données de la colonne, de les trier ou de les filtrer. Afin de se
composer le tableau de ses rêves, on peut aussi masquer une colonne ou
en ajouter une autre à droite. 

Pour vous aider à retrouver facilement une fiche, chaque fiche peut
être associée à une vignette (souvent la photo représentative du bien
concerné). 

Cliquer sur une ligne du tableau permet d'ouvrir la fiche
correspondante.

Le menu global de cette page permet de rafraîchir la liste, de
sélectionner une série de fiches pour les supprimer, d'obtenir un
fichier CSV des fiches pour alimenter un tableur ou d'obtenir la carte
géographique des biens (dont la localisation est connue).

## Persistance de vos données

Vos fiches sont stockées dans votre navigateur. Toutefois, si votre
navigateur juge qu'elles prennent trop de place, il en supprimera
certaines et de façon non contrôlable, pour faire de la place. Vous
pouvez refuser cela en acceptant que vos données soient stockées de
façon persistante comme vous le permet le dialogue qui apparaît si
vous n'avez pas choisi cette option.

## Synchronisation

Si vous disposez de droits améliorés, vous pouvez également importer
ou exporter vos fiches dans ou depuis un fichier. Cela permet ainsi
de sauvegarder ou restaurer son entier jeu de fiches.

Vous pouvez aussi les synchroniser c'est-à-dire de les stocker sur le
serveur de TRINV afin de les partager avec vos autres appareils
(téléphone, tablette, autre navigateur, etc.). Attention, la
synchronisation nécessite quelques précautions et de bien réfléchir à
l'ordre des opérations.


