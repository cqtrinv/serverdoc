---
layout: doclayout.liquid
pageTitle: Fiches favorites
tags: [ 'ContextualDoc' ]
---

Cette page, assez technique, permet de rechercher une parcelle par son
identifiant. 

L'identifiant d'une parcelle est codée comme AAAAA-000-AA-999 où AAAAA
est le code INSEE de la commune, 000 un nombre, AA est le nom de la
feuille de cadastre (une ou deux lettres) et 999 le numéro de la
parcelle au sein de cette feuille.

Le nombre 000 est souvent égal aux trois derniers chiffres du code
INSEE d'une ancienne commune fusionnée.
