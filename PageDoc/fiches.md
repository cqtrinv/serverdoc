---
layout: doclayout.liquid
pageTitle: Fiches multiples
tags: [ 'ContextualDoc' ]
---

TRINV peut analyser une page de résultats de 
[Nestoria](https://nestoria.fr/) ou 
[MoteurImmo](https://moteurimmo.fr/) ou
[SeLoger](https://seloger.com/).

La page résultante présente les grandes caractéristiques des annonces
analysées. Attention, parfois, seulement une partie des annonces ont
pu être analysées.
 
Cliquer sur une des annonces révèle un menu permettant de créer une
fiche à partir de l'annonce, d'ignorer l'annonce (elle apparaît alors
sur fond gris). Lorsqu'une annonce est ignorée, TRINV le retient et la
présentera comme ignorée les prochaines fois. Le menu disparaît après
un nouveau clic. Une annonce ignorée peut aussi ne plus être ignorée
et donc à nouveau reconnue.

On peut aussi tenter de rapprocher l'annonce des annonces existantes,
c'est-à-dire chercher, parmi les fiches existantes, une fiche ayant
une description similaire. Les fiches candidates sont alors affichées
et vous pouvez choisir de fusionner cette nouvelle annonce avec une
annonce déjà connue.

Les fiches sont bordées de pointillés lorsque nouvelles, bordées d'une
ligne solide lorsque déjà connues et sur fond gris lorsqu'ignorées.

Ainsi, vous pouvez surveiller les annonces d'un site aggrégateur, et

- visualiser les annonces nouvelles et les mémoriser en des fiches
- ignorer certaines annonces car inintéressantes
- ou bien chercher si l'annonce n'est que la réédition d'une annonce que vous connaissiez déjà!




