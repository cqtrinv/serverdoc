---
layout: doclayout.liquid
pageTitle: Mémoriser une URL
tags: [ 'ContextualDoc' ]
---

Ce formulaire permet de mémoriser une URL et de créer une fiche
associée à cette URL. La fiche peut être complétée, annotée, enrichie
en photos, etc.

## Que mémoriser ?

L'URL peut mener à une agence immobilière ou à une requête vers un
aggrégateur pour obtenir en retour une sélection de biens. Voici,
des exemples de telles requêtes:

```
https://www.nestoria.fr/erquy/immobilier/vente
https://www.seloger.com/list.htm?&idtypebien=2&idtt=2,5&naturebien=1,2,4&ci=140117
```

## Mémoriser une annonce immobilière

Pour mémoriser une annonce immobilière, mieux vaut utiliser
l'[extension TRINVext](/comment_utiliser_l_extension_trinvext)
qui analysera l'annonce et en extraiera les principales
caractéristiques. C'est fort utile puisque la plupart des annonces
immobilières disparaissent assez rapidement, l'URL y conduisant 
devenant obsolète.

