---
layout: doclayout.liquid
pageTitle: Informations personnelles
tags: [ 'ContextualDoc' ]
---

Cette page vous permet de voir ou modifier vos informations
personnelles. Celles-ci ne comportent que votre pseudo et votre courriel.

Votre courriel est votre identifiant et ne peut être modifié! 

Votre pseudo (que vous pouvez modifier ci-dessous tout en restant
civil, courtois et poli) pourrait, dans un hypothétique futur, être
visible publiquement. Il pourrait servir à qualifier les
informations que vous souhaiteriez partager.

Pour les utilisateurs dotés de super-pouvoirs, ceux-ci sont visiblement
affichés. Ces super-pouvoirs sont:

- être exempté de publicités
- pouvoir partager ses fiches sur plusieurs appareils ou utilisateurs


