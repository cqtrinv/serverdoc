---
layout: doclayout.liquid
pageTitle: Synchronisation des fiches 
tags: [ 'ContextualDoc' ]
---

Synchroniser ses fiches permet de gérer les fiches que l'on détient
sur le navigateur que vous utilisez présentement et celles qui se
trouvent stockées sur le serveur de TRINV. La première ligne du
tableau permet de réaliser les actions les plus courantes sur
l'ensemble des fiches mais vous pouvez également traiter vos fiches
une par une. Au début, choisissez le mode détaillé pour mieux saisir les
conséquences du mode abrégé qui agit sur toutes les fiches en même temps.

Un système de version et de date de dernière modification permet de
ne proposer que les seules actions possibles par fiche.

Mais la synchronisation demande un super-pouvoir que vous n'avez pas
nécessairement.

## Deux modes

Il existe deux modes de synchronisation: le mode simple et le mode avancé.

## Mode abrégé

Le mode abrégé permet de rendre le nuage exactement conforme aux
fiches que vous détenez dans votre navigateur (effacement des fiches
du serveur et copie de vos fiches vers le serveur). Inversement, de
rendre vos fiches locales à votre navigateur conformes à ce qui se
trouve dans le nuage (effacement de vos fiches locales et remplacement
par les fiches du serveur). 

Ce mode permet donc de faire de multiples modifications à ses fiches
puis de tout archiver dans le nuage. Si vous devez changer de
navigateur, vous pouvez tout récupérer dans l'état où vous l'aviez
sauvegardé.

Attention c'est un mode dangereux car, dans les deux cas, on écrase le
récipiendaire (ici ou nuage) avec le contenu de l'émetteur (ici ou
nuage). À manier donc avec précaution

## Mode détaillé

Dans le mode détaillé, la synchronisation est effectuée fiche par
fiche. On peut ne voir que les fiches susceptibles d'être
synchronisées ou voir toutes les fiches même celles qui sont à jour
des deux côtés.

### Cas des fiches n'existant que dans votre navigateur

Vous pouvez les supprimer ou les exporter vers le serveur c'est
le cas notamment des fiches que vous venez de créer.

Si vous souhaitez disposer de vos fiches dans un autre navigateur
(tablette, téléphone ou autre ordinateur) vous devez d'abord les
exporter puis changer de navigateur afin de les y importer.

### Cas des fiches n'existant que dans le serveur

Vous pouvez les supprimer ou les importer dans votre navigateur. Cela
peut être le cas de fiches que vous avez malencontreusement supprimées
de votre navigateur et que vous souhaitez restaurer. Cela peut aussi
être le cas de fiches inutiles que vous souhaitez voir disparaître du
serveur de TRINV.

Importer toutes les fiches permet d'en disposer dans plusieurs
navigateurs. 

## Cas des fiches existant dans les deux endroits

Là, vous devrez probablement réfléchir, fiche par fiche, afin de soit
mettre à jour la version distante (si vous avez modifié la version
locale) ou mettre à jour la version locale (si, après avoir modifié la
fiche sur une autre machine, vous l'avez exporté sur le serveur). 
