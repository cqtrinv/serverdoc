---
layout: doclayout.liquid
pageTitle: Carte des DPE
tags: [ 'ContextualDoc' ]
---

Cette carte affiche tous les DPE de la commune concernée. La liste de
ces DPE ainsi que leurs principales caractéristiques (DPE, GES, date,
surface et adresse) sont listées sous la carte. Les DPE étant
obligatoires pour la mise en vente d'un bien, leur connaissance permet
souvent de retrouver les adresses des biens en vente. Il y a cependant
des [inconvénients](/a_propos_des_dpes) à leur usage.

Cliquer sur ces lignes ouvre le marqueur correspondant. Les marqueurs
géolocalisés des DPE sont bleus.

Il est également possible d'afficher vos fiches favorites relatives à
la commune concernée. Vous pouvez apparier une fiche et un DPE. La
sélection d'un DPE ou d'une fiche ouvre une boîte d'appariement;
lorsque DPE et fiche sont sélectionnées, un bouton d'association
apparaît. Le marqueur prend alors les deux couleurs.
