// Common (client and server) library of utilities

export function sanitizeCountyname (s) {
    let result = s.replace(/[^-' a-z0-9]/ig, '');
    return result;
}

export function sanitizeInteger (s, defaultValue = 0) {
    if ( s ) {
        let result = s.replace(/[^0-9]/g, '');
        return result;
    } else {
        return defaultValue;
    }
}

export function sanitizeFloat (s, defaultValue = 0) {
    if ( s ) {
        const f = Number.parseFloat(s);
        if ( ! Number.isNaN(f) ) { 
            return f;
        }
    }
    return defaultValue;
}

export function sanitizeEmail (s) {
    let result = s.replace(/[^-a-z.0-9@]/ig, '');
    return result;
}

export function sanitizeText (s) {
    if ( s ) {
        let result = s.replace(/[^-\w.\d :/]/ig, '');
        return result;
    } else {
        return undefined;
    }
}

export function sanitizeToken (s) {
    // Examples: "auth0|5efc4186bb331300139b7a97"
    //           "google-oauth2|112687159521299245709"
    let result = s.replace(/[^-a-z0-9|]/ig, '');
    return result;
}

export function sanitizePseudo (s) {
    let result = s.replace(/[^ _a-z0-9|-]/ig, '');
    return result;
}

export function sanitizeInseeid (s) {
    if ( s ) {
        let result = s.replace(/[^a-z0-9]/ig, '');
        if ( result.match(/^\w{5}$/) ) {
            return result;
        }
    }
    return undefined;
}

export function sanitizeDay (s) {
    if ( s ) {
        let result = s.replace(/[^0-9-]/ig, '');
        if ( result.match(/^\d{4}-\d{2}-\d{2}$/) ) {
            return result;
        }
    }
    return undefined;
}

// reverse may return something as id: '56116_a022_00011' but
// unfortunately all parcelles in Locmariaquer have two letters [AB]?

export function sanitizeCadastreid (s) {
    if ( s ) {
        let result = s.replace(/[^a-z0-9_-]/ig, '');
        result = result.toUpperCase().replace(/_/g, '-');
        if ( result.match(/^\w{5}-\d+-?\w+-\d+$/) ) {
            return result;
        }
    }
    return undefined;
}

export function sanitizeSurface (s) {
    if ( s ) {
        // Allow integers or integer..integer or integer[-:]integer 
        let result = s.replace(/[^.:0-9-]/ig, '');
        // normalize separator to a single dash:
        result = result.replace(/[.]+/, '-');
        result = result.replace(/:/, '-');
        result = result.replace(/-{2,}/, '-');
        return result;
    }
}

export function sanitizeBool (s) {
    let result = !!s;
    return result;
}

export function sanitizeUUID (s) {
    if ( s ) {
        let result = s.replace(/[^a-f0-9|]/ig, '');
        if ( result.length !== 32 ) {
            return undefined;
        }
        return result.toLowerCase();
    } else {
        return undefined;
    }
}

export function normalizeUUID (uuid) {
    if ( uuid ) {
        uuid = uuid.replace(/-/g, '');
        const result = uuid.slice(0, 8) + '-' +
              uuid.slice(8,12) + '-' +
              uuid.slice(12,16) + '-' +
              uuid.slice(16,20) + '-' +
              uuid.slice(20,32);
        return result.toLowerCase();
    } else {
        throw new Error('missing UUID');
    }
}
// normalizeUUID('8e47ef44-c7de-4ef5-aa6e-b7c59c33c37d');
// normalizeUUID('8e47ef44c7de4ef5aa6eb7c59c33c37d');

export function normalizeCadastreid (cadastreid) {
    function normalizeNum (s) {
        s = s.replace(/^0*/, '');
        if ( s.length <= 3 ) {
            return ('000' + s).slice(-3);
        }
        return s;
    }
    if ( cadastreid ) {
        let m = cadastreid.match(/^(\w{5}-\w{3}-\w+-)(\d+)$/);
        if ( m ) {
            let result = m[1] + normalizeNum(m[2]);
            return result;
        }
        let result = cadastreid.replace(/-/g, '');
        const countyid = result.slice(0,5);
        const subcountyid = result.slice(5,8);
        const rest = result.slice(8);
        let letters;
        let num;
        if ( result === cadastreid ) {
            // cadastreid was in condensed form (without dashes):
            m = rest.match(/^([a-z]+)(\d+)$/i);
            if ( m ) {
                letters = m[1];
                num = normalizeNum(m[2]);
            } else {
                letters = rest.slice(0, -4);
                num = normalizeNum(rest.slice(-4));
            }
        }
        result = countyid + '-' +
            subcountyid + '-' +
            letters + '-' +
            num;
        return result;
    }
    return undefined;
}
// Tests with jasmine in spec/17-cadasstre-spec.js

export function smallify (s, limit=20, other='sans titre') {
    if ( s && typeof s === 'string' ) {
        if ( s.length > limit ) {
            return s.substring(0, limit) + '...';
        } else if ( s === '' ) {
            return other;
        } else {
            return s;
        }
    } else {
        return other;
    }
}

export function smallifyURL (s) {
    const url = new URL(s);
    let pathname = url.pathname;
    pathname = pathname.replace(/\/*$/, '');
    pathname = pathname.replace(/[.]\w+$/, '');
    const words = pathname.split('/').reverse();
    for ( let word of words ) {
        if ( ! word.match(/^\d+$/ ) ) {
            return smallify(word);
        }
    }
    return `...${smallify(pathname)}`;
}

export function sanitizeURL (s) {
    if ( s) {
        try {
            const url = new URL(s);
            return url.toString();
        } catch (exc) {
            /* eslint no-unused-vars: "off" */
            return undefined;
        }
    } else {
        return undefined;
    }
}

export function sanitizePathname (s) {
    if ( s ) {
        let result = s.replace(/[^/.a-z0-9_+-]/ig, '');
        return result;
    }
    return undefined;
}

/**
   Check if network is present
*/

export function isNetworkPresent () {
    if ( typeof navigator === 'undefined' ) {
        // on Server:
        return true;
    } else {
        return navigator.onLine;
    }
}

/** 
   Decode query parameters and store it in the eventRequest object.

   @param {PageLoadEvent} event
   @return {Object}
*/

export function decodeSearchParams (event) {
    const result = {};
    const urlsearchparams = event.url.searchParams;
    for ( let key of urlsearchparams.keys() ) {
        result[key] = urlsearchparams.get(key);
    }
    return result;
}

/**
   Sleep for n seconds
*/

export async function sleep (seconds, answer=true) {
    return new Promise((resolve) => {
        setTimeout(() => resolve(answer), 1000 * seconds);
    });
}

/**
   Limit a computation for n seconds

   @return {value} of Promise
   @throw if the promise does not resolve in less than n seconds
*/

export async function timeout (promise, seconds, answer=false) {
    return Promise.race([
        new Promise((resolve, reject) =>{
            setTimeout(() => reject(answer), 1000 * seconds);
        }),
        promise
    ]);
}
// tests
//await timeout(sleep(1, true), 2) === true
//try { await timeout(sleep(2, false), 1, true) } catch (exc) { exc }

/**
    Reduce the size of oversized strings.
    This is used as second argument of JSON.stringify()
*/

export function shortenStrings (key, value) {
    const limit = 67;
    if ( typeof value === 'string' &&
         value.length > limit &&
         true ) {
        return value.slice(0, limit) + '...';
    }
    return value;
}

export function ensureNumber (s) {
    if ( typeof s === 'number' ) {
        return s;
    } else if ( typeof s === 'string' &&
                s.match(/^[+-]?\d+([.]\d*)?$/) ) {
        return +(s);
    } else {
        return undefined;
    }
}

/**
   ensureDate
   This is needed since JSON date are strings
   but ldbobjects require DateTime.

*/

export function ensureDate (s) {
    if ( s instanceof Date ) {
        return s;
    } else if ( typeof s === 'number' ) {
        return new Date(s);
    } else if ( typeof s === 'string' ) {
        try {
            return new Date(Date.parse(s));
        } catch (exc) {
            //$log.error('ensureDate', exc.toString());
            return new Date(Date.now());
        }
    } else {
        return new Date(Date.now());
    }
}
/*
  ensureDate(new Date())
  ensureDate(Date.now());
  ensureDate('2025-02-05T12:35Z')
  ensureDate()
*/

export function ensureEpoch (s) {
    if ( s instanceof Date ) {
        return s.getTime();
    } else if ( typeof s === 'number' ) {
        return s;
    } else if ( typeof s === 'string' ) {
        try {
            return Date.parse(s);
        } catch (exc) {
            //$log.error('ensureEpoch', exc.toString());
            return Date.now();
        }
    } else {
        return Date.now();
    }
}
/*
  ensureEpoch(new Date())
  ensureEpoch(Date.now())
  ensureEpoch('2025-02-05T12:35Z')
  ensureEpoch()
 */

// end of lib.mjs
