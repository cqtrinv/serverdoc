// A small logger storing traces in memory
// May be used on both server and client sides.

//import { shortenStrings } from './lib.mjs';

/**
    Reduce the size of oversized strings.
    This is used as second argument of JSON.stringify()
*/

function shortenStrings (key, value) {
    const limit = 67;
    if ( typeof value === 'string' &&
         value.length > limit &&
         true ) {
        return value.slice(0, limit) + '...';
    }
    return value;
}

/**
   config = {
       size: 50            // maximum number of kept logs
       alsoconsole: true   // also log onto the console
       server: true
   }


   In files *.server.js use

import { Logger } from '$lib/common/logger.mjs';
const $log = new Logger();

   in *.svelte or *.js files use

 import { log } from '$lib/stores.mjs';
 $log.debug();

*/

export class BaseLogger {
    constructor (config={}) {
        this.lines = [{
            level: 'log',
            t: new Date(),
            args: ["initialisation"],
        }];
        this.config = Object.assign({}, BaseLogger.defaultConfig, config);
    }
    // Useful for Svelte to detect if the log has changed:
    freshness () {
        return this.lines[this.lines.length-1].t;
    }
    _add (level, ... args) {
        const t = new Date();
        if ( this.config.server ) {
            console.log(t, ... args);
            return;
        } 
        if ( this.config.alsoconsole) {
            // Playwright console is not Firefox console:
            /*
            if ( onFirefox() ) {
                args = args;
            } else if ( onChrome() ) {
                args = args.map(o => JSON.stringify(o, shortenStrings, 2));
            }*/
            console.log(t, ... args);
        }
        const self = this;
        args = args.map(arg => self._clone(arg, 3));
        this.lines.push({level, t, args});
        this.lines = this.lines.slice(-this.config.size);
    }
    _clone (arg, depth) {
        if ( this.config.preferJSON ) {
            return JSON.stringify(arg, shortenStrings, 2);
        } else {
            return clone(arg, depth);
        }
    }
    // debug info warn error fatal
    debug (... args) {
        this._add('debug', ... args);
    }
    info (... args) {
        this._add('info', ... args);
    }
    warn (... args) {
        this._add('warn', ... args);
    }
    error (... args) {
        this._add('error', ... args);
    }
    get (level='(debug|info|warn|error)') {
        const re = new RegExp(level);
        return this.lines.filter(line => line.level.match(re));
    }
    getHTML () {
        let result = [];
        result.push(`<ul>`);
        for ( let line of this.lines ) {
            result.push(`<li>${line.t} ${line.level.toUpperCase()} `);
            for ( let arg of line.args ) {
                result.push(`<span>${arg}</span>`);
            }
            result.push(`</li>`);
        }
        result.push(`</ul>`);
        return result.join('');
    }
    clear () {
        this.lines = [];
    }
    void () {
        return new VoidLogger();
    }
}
BaseLogger.defaultConfig = {
    size: 500,
    preferJSON: true,
};

/**
   Special version for client

   Use as:

   import { Logger } from '$lib/common/logger.mjs';
   const $log = new Logger();
   $log.freshness();

   or
   import { log } from '$lib/stores.mjs';
   $log.freshness();
   
*/

let theLogger;

export class Logger extends BaseLogger {
    constructor (config={}) {
        super(config);
        if ( theLogger ) {
            return theLogger;
        }
        this.lines = [{
            level: 'log',
            t: new Date(),
            args: ["initialisation"]
        }];
        this.config = Object.assign({}, Logger.defaultConfig, config);
        theLogger = this;
        // Useful for DEBUG:
        if ( typeof window === 'object' ) {
            window.__TRINV_logger = theLogger;
        }
    }
}

/**
   Special version for server

   Use as:

   import { ServerLogger } from '$lib/common/logger.mjs';
   const $log = new ServerLogger();
   $log.freshness();
   
*/

let theServerLogger = undefined;

export class ServerLogger extends BaseLogger {
    constructor (config={ server: true }) {
        super(config);
        if ( theServerLogger ) {
            return theServerLogger;
        }
        theServerLogger = this;
    }    
}

/**
   Special version that does not log anything.

   Use as       const $log = $log.void();
   
*/

let theVoidLogger;

class VoidLogger extends BaseLogger {
    constructor (config={ voidlogger: true }) {
        super(config);
        if ( theVoidLogger ) {
            return theVoidLogger;
        }
        theVoidLogger = this;
        return theVoidLogger;
    }
    _add (/*level, ... args*/) {}
    void () {
        return new VoidLogger();
    }
}

/**
   onChrome: Playwright uses Chrome but with a crippled console.log
   so use JSON.stringify explicitly.

function onChrome () {
    return false;  ///////////////////// TEMP
    /*
    return ( typeof window !== 'undefined' &&
             window.navigator &&
             window.navigator.vendor &&
             window.navigator.vendor.match(/^Google/i) &&
             true );
}
*/

/**
   onFirefox

function onFirefox () {
    return false;  ///////////////////// TEMP
    /*
    return ( typeof window !== 'undefined' &&
             window.navigator &&
             window.navigator.userAgent &&
             window.navigator.userAgent.match(/Firefox/) &&
             true );
}
*/

/**
   We print on the console if we are on the server or if we are in dev
   mode.

   NOTA: Don't use import { dev, browser } from '$app/environment'
   since it cannot be handled by Jasmine in test.html

function useConsole () {
    function devMode () {
        return false; // TEMP
    }
    if ( typeof window !== 'undefined' ) {
        return devMode();
    } else {
        // we are on the server:
        return true;
    }
}
*/

function clone (x, depth=2) {
    if ( typeof x === 'object' &&
         x !== null &&
         depth > 0 ) {
        const result = {};
        for ( const key of Object.keys(x) ) {
            result[key] = clone(x[key], depth-1);
        }
        return result;
    } else if ( Array.isArray(x) ) {
        const result = x.map(item => clone(item, depth-1));
        return result;
    } else {
        return x;
    }
}

// end of logger.mjs
