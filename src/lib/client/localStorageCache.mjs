// -*- coding: utf-8 -*-
// Time-stamp: "2023-02-16 17:00:26 queinnec"

import { log } from '$lib/stores.mjs';
import { get } from 'svelte/store';

/**
   A localStorage based cache.
   Options are:
      prefix:  of the keys used to retrieve values 

   Caches are multiplexed within the localStorage. Currently there are
   the 'urls' cache, the 'counties' cache, the 'adverthistory' cache.
   All keys are prefixed by 'TRINV:' (Google also use the localStorage
   with keys such as google_experiment*, goog_pem_mod, google_*)

   # This cache has a single entry:
   TRINV:counties:all => JSON:[[ "56034", "CARNAC" ], ... ]

   # The cache of favorite urls:
   TRINV:urls:<UUID> => JSON:{ uuid, url, name, date }
   ...

   TRINV:adverthistory:<UUID> => JSON:<graphql query>
   

NOTA: Display the content of the localStorage in the browser $log.

*/

export class LocalStorageCache {
    constructor (options={}) {
        try {
            this.localStorage = window.localStorage;
            this.prefix = options.prefix || '';
            if ( this.prefix.length > 0 &&
                 ! this.prefix.match(/:$/) ) {
                this.prefix += ':';
            }
            this.counter = this.localStorage.length;
        } catch (exc) {
            get(log).error(`No localStorage here`, {exc});
        }
    }
    clear () {
        if ( this.localStorage ) {
            return this.localStorage.clear();
        }
    }
    setItem (key, value) {
        if ( this.localStorage ) {
            const newkey = `TRINV:` + this.prefix + key;
            let result;
            if ( typeof value === 'string' ) {
                const s = 'STRING:' + value;
                result = this.localStorage.setItem(newkey, s);
                this.counter++;
                return result;
            } else if ( typeof value === 'object' ) {
                const s = 'JSON:' + JSON.stringify(value);
                result = this.localStorage.setItem(newkey, s);
                this.counter++;
                return result;
            } else {
                const msg = `Non serializable value`;
                get(log).error(msg, key, value);
                throw new Error(msg);
            }
        }
    }
    getItem (key) {
        if ( this.localStorage ) {
            const newkey = `TRINV:` + this.prefix + key;
            const v = this.localStorage.getItem(newkey);
            if ( v ) {
                const m = v.match(/^(STRING|JSON):(.*)$/);
                if ( m && m[1] === 'STRING' ) {
                    return m[2];
                } else if ( m && m[1] === 'JSON' ) {
                    return JSON.parse(m[2]);
                } else {
                    get(log).error(`non deserializable value`, key);
                    return undefined;
                }
            }
        }                
    }
    allItems () {
        if ( this.localStorage ) {
            const re = new RegExp(`^TRINV:` + this.prefix);
            const results = [];
            const n = this.localStorage.length;
            for ( let i = 0 ; i < n ; i++ ) {
                const key = this.localStorage.key(i);
                if ( key.match(re) ) {
                    const shortkey = key.replace(re, '');
                    results.push(this.getItem(shortkey));
                }
            }
            return results;
        }
    }
    removeItem (key) {
        if ( this.localStorage ) {
            const newkey = `TRINV:` + this.prefix + key;
            const result = this.localStorage.removeItem(newkey);
            this.counter--;
            return result;
        }
    }
    key (index) {
        if ( this.localStorage ) {
            return this.localStorage.key(index);
        }
    }
    length () {
        if ( this.localStorage ) {
            return this.localStorage.length;
        }
    }    
}

// end of localStorageCache.mjs
