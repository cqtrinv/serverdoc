// A (localStorage) cache for the counties mentioned by some user.

/**
   The cache held in the local storage is an array.
   The first element is the version (an integer).
   The other elements are records that is, arrays with a key as first element.

   Record is [ key, data, optional... ]
*/

import { LocalStorageCache } from '$lib/client/localStorageCache.mjs';

const caches = {};

export class LastCache {
    constructor (prefix, version) {
        if ( ! caches[prefix] ) {
            this.version = version;
            this.cache = new LocalStorageCache({ prefix });
            caches[prefix] = this;
            this.shouldUpdate = false;
            this.retrieve();
        }
        return caches[prefix];
    }
    defaultValue () {
        return [];
    }
    store () {
        const d = [ this.version, ...this.records];
        this.cache.setItem('all', d);
        return this;
    }
    retrieve () {
        const d = this.cache.getItem('all');
        if ( d ) {
            const v = d[0];
            this.records = d.slice(1);
            if ( Number.isInteger(v) ) {
                if ( v < this.version ) {
                    this.shouldUpdate = true;
                    // Caution: update may be async!
                    this.records = this.update(v, this.records);
                    this.store();
                }
            } else {
                // missing version (aka version = -1), store with version:
                this.store();
            }
        } else {
            this.records = this.defaultValue();
            this.store();
        }
    }
    find (key) {
        const record = this.records.find(r => r[0] === key);
        if ( record ) {
            return record[1];
        }
        return undefined;
    }
    update (v, oldrecords) {
        /* eslint no-unused-vars: "off" */
        throw new Error('absent update method');
    }
}
