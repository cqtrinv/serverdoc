// -*- coding: utf-8 -*-

// This library provides some abstract classes and utilities to
// represent database entities on the client side.

//import { encodeBody } from '$lib/client/lib.mjs';
import queryString from 'query-string';
import { log } from '$lib/stores.mjs';
import { get } from 'svelte/store';
const $log = get(log);

/**
   Request service with a GET query string built from queryobject, the
   result is processed with creator (depending on the type of the
   result (array or object).

   @param {string} service - name of the service
   @param {object} queryobject - query parameters
   @param {function} creator - 
   @returns Promise
*/

export async function getItems (service, queryobject, creator) {
    let url = `/api/${service}.json`;
    if ( Object.keys(queryobject).length > 0 ) {
        //url += '?' + encodeBody(queryobject);
        url += '?' + queryString.stringify(queryobject);
    }
    const response = await fetch(url, {
        method: 'GET',
        mode: 'cors',
        redirect: 'follow',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });
    if ( response.ok ) {
        let items = await response.json();
        if ( Array.isArray(items) ) {
            items = items.map(creator);
        } else {
            items = creator(items);
        }
        return items;
    } else {
        let error;
        try {
            error = await response.json();
            error._code_ = response.status;
        } catch (exc) {
            $log.debug('getItems', exc.toString());
            error = response.status;
        }
        throw error;
    }
}

/**
   Request service with a POST request and a JSON bodyobject, the
   result is processed with creator (depending on the type of the
   result (array or object).

   @param {string} service - name of the service
   @param {object} bodyobject - body JSON parameters
   @param {function} creator - 
   @returns Promise
*/

export async function postItems (service, bodyobject, creator) {
    const response = await fetch(`/api/${service}.json`, {
        method: 'POST',
        mode: 'cors',
        redirect: 'follow',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(bodyobject)
    });
    if ( response.ok ) {
        let items = await response.json();
        if ( Array.isArray(items) ) {
            items = items.map(creator);
        } else {
            items = creator(items);
        }
        return items;
    } else {
        let error;
        try {
            error = await response.json();
        } catch (exc) {
            $log.debug('postItems', exc.toString());
            error = response.status;
        }
        throw new Error(error);
    }
}


export class RemoteObject {
}

export function checkFieldAbsence (fields) {
    return function (js) {
        if ( ! js ) {
            throw `Void object`;
        }
        fields.forEach(field => {
            if ( ! (field in js) ) {
                throw `Missing field ${field}`;
            }
        });
    };
}

/** 
     formatNumber 
*/

export function formatNumber (n) {
    function power1000 (i) {
        let result = 1;
        while ( i-- > 0 ) {
            result *= 1000;
        }
        return result;
    }
    let result = '';
    for ( let i=4 ; i>=0 ; i-- ) {
        let limit = power1000(i);
        if ( n >= limit ) {
            result += Math.floor(n/limit) + ' ';
            n = n%limit;
        }
    }
    return result;
}
/*
 formatNumber(123)
 formatNumber(12345)
 formatNumber(12345678)
*/

// end of client/classes/utils.mjs
