// Client class for the Person table

import { RemoteObject, postItems, getItems, checkFieldAbsence }
  from '$lib/client/classes/utils.mjs';
//import { UserconfCache } from '$lib/client/userconfCache.mjs';
import { get } from 'svelte/store';
import { log } from '$lib/stores.mjs';
const $log = get(log);

export class Person extends RemoteObject {
    constructor (js) {
        super();
        Object.assign(this, js);
    }
    // Turn the answer of Auth0 into a Person
    static createFromAuth0 (js) {
        // Ignore most fields and only keep those coming from Auth0:
        const fieldNames = [
            'email', 'email_verified', 'nickname', 'sub' ];
        checkFieldAbsence(fieldNames)(js);
        const njs = {
            email: js.email,
            checkedemail: js.email_verified,
            pseudo: js.nickname,
            token: js.sub, // the Auth0 user identifier
            data: {}
        };
        const p = new Person(njs);
        //$log.debug('createFromAuth0', p);
        return p;
    }
    // Turn the answer of /api/person.json into a Person
    static createFromJSON (js) {
        const p = new Person(js);
        p.data = Object.assign({}, js.data);
        //$log.debug('createFromJSON', p, js);
        return p;
    }
    // 
    isAdmin () {
        if ( this.data && this.data.admin ) {
            return this.data.admin;
        }
        return false;
    }
}

/**
   Ask the server in order to identify the user by its sessionID.
   Returns an object suitable to be the value of $person. That is an
   object with the following fields: 
      id, token, pseudo, email, checkedemail
   
   @returns {Object} - $person

*/

/*
export async function whoAmI () {
    let result = undefined;
    const response = await fetch('/api/person.js', {
        method: 'POST',
        mode: 'cors',
        redirect: 'follow',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ kind: 'whoami' })
    });
    if ( response.ok ) {
        const json = await response.json();
        result = create$Person(json);
    }
    return result;
}
*/

export async function whoAmI () {
    let result = undefined;
    try {
        result = await getItems('person', {}, create$Person);
        if ( ! result || ! result.token ) {
            result = undefined;
        }
    } catch (exc) {
        $log.error('whoAmI', exc.toString());
        result = undefined;
    }
    return result;
}

/** 
    Create an object coming from /api/person.json.js suitable to
    become the value of $person. That is an object with the following
    fields: id, token, pseudo, email, checkedemail, data

    @returns {Object} - $person
    
 */

export function create$Person (json) {
    const p = json.person;
    const person = Person.createFromJSON({
        id: p.id,
        token: p.token,
        pseudo: p.pseudo,
        email: p.email,
        checkedemail: p.checkedemail,
        data: p.data
    });
    //Object.assign(person, json.data);
    //$log.debug('create$Person', person, json);
    return person;
}

/**
   Create a Person row into the database based on the information
   received from Auth0. This is useful after enrolling a new user via
   Auth0.

   Returns an object suitable to be the value of $person. That is an
   object with the following fields: id, token, pseudo, email,
   checkedemail
   
   NOTA: this also returns the current person.data information
   rawdata signed with cryptdata.
   
   @returns {Object} - $person
*/

async function storePerson (person) {
    try {
        const params = {
            kind: 'findOrCreate',
            pseudo: person.pseudo,
            token: person.token,
            email: person.email,
            checkedemail: person.checkedemail
            // use default 'data' column
        };
        const result = await postItems('person', params, create$Person);
        //$log.debug('storePerson', result);
        return result;
    } catch (exc) {
        return { error: exc };
    }
}

/**
   Given the answer of Auth0, find or create a Person in the database.

   Returns an object suitable to be the value of $person. That is an
   object with the following fields: id, token, pseudo, email,
   checkedemail, data
   
   @returns {Object} - $person

   NOTA: should do this only for new users! How to distinguish them ???

*/

export async function createPerson (user) {
    // person comes from Auth0, extract only the needed information:
    const person = Person.createFromAuth0(user);
    // while newperson comes from /api/person.json
    const newperson = await storePerson(person);
    // if newperson.newUser, create folder and other dependent information .....
    return create$Person({person: newperson});
}

/**
   Modify the 'pseudo' field of a Person within the database.

   Returns an object suitable to be the value of $person. That is an
   object with the following fields: id, token, pseudo, email,
   checkedemail
   
   @returns {Object} - $person
*/

export async function modifyPerson (person, parms) {
    let modifications = Object.assign({
        kind: 'modify'
    }, person, parms);
    delete modifications.data;
    const result = await postItems('person', modifications, create$Person);
    return result;
}

// end of client/classes/Person.mjs
