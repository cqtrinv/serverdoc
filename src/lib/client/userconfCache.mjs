// a (localstorage) cache for user configuration
// and mainly the 'nopub' feature.

/**
   Publicity are always displayed unless $person.showpub is false.
   This boolean is recorded in UserconfCache held in localStorage.

   The descriptor is held in the client but created by the server.
   The client only knows the public key to verify the descriptor.
   The 'end' parameter means stop pub and other special rights after 'end'.

   TRINV:userconf:all => JSON:[version] 
                         JSON:[version,{
              user: { pseudo: 'mypseudo',
                      email: 'x.y@gmail.com',
                      checkedemail: bool,
                      token: "google-oauth2|XXX",
                      rawdata: { ... },             
                      cryptdata: 'sigbase64',
                      data: {        // same as rawdata but verified!
                              end: epoch
                              syncid: 'uuid'
                              admin: bool,
                              seedpe: bool,
                              seepc: bool,
                              export: bool,
                      }
              },
              badges: {                          // see userBadges.mjs
                  "attempts": 23,
                  "successes": 12,
              }
          }
        ]

   theVerifyer holds (if imported) the key to verify
   the 'end' parameter. When verified, the 'end' parameter is just
   a number (an epoch) that is a number of milliseconds.
   'sig' is a base64 string holding the signature of the text of rawdata.

https://stackoverflow.com/questions/75277447/window-crypto-subtle-how-we-can-use-encrypt-decrypt-method-if-we-generate-pu
https://www.geeksforgeeks.org/node-js-crypto-publicdecrypt-method/
http://travistidwell.com/jsencrypt 
https://www.samiwell.eu/javascript/js-encrypt-data-using-public-key

Additional properties may be stored in this cache (see userBadges.mjs)
even for non authenticated users.
*/

import { LastCache } from '$lib/client/lastCache.mjs';
import { person, log, hostbase } from '$lib/stores.mjs';
import { get } from 'svelte/store';
const $log = get(log).void();
import { ensureNumber, normalizeUUID } from '$lib/common/lib.mjs';
import { toUint8Array } from 'js-base64';
import { create$Person } from '$lib/client/classes/Person.mjs';

const version = 1;
let theUserconfCache;

export class UserconfCache extends LastCache {
    constructor () {
        if ( ! theUserconfCache ) {
            theUserconfCache = super('userconf', version);
        }
        
        return theUserconfCache;
    }
    defaultValue () {
        return [{}];
    }

    // setUser(_, true) is ONLY called from +layout.svelte and
    // normally only once!
    // setUser(_) is only called once from +layout.js every time a
    // page is reloaded. 
    setUser (user, check=false) {
        $log.debug('setUser1', check, user, JSON.stringify(user.data));//DEBUG
        if ( user ) {
            this.records[0].user = user;
            this.store();
            //$log.debug('setUser2', user);//DEBUG
            if ( check ) {
                this.checkRegularly();
            }
        }
    }

    getUser () {
        const user = this.records[0].user;
        if ( user ) {
            $log.debug('getUser', user, JSON.stringify(user.data));//DEBUG
            // adjoin badges if any:
            user.badges = this.badges();
            return user;
        } 
        return undefined;
    }
    
    clearUser () {
        $log.debug('clearUser');//DEBUG
        delete this.records[0].user;
        this.store();
    }

    async refreshUser () {
        return await refreshPersonData.bind(this)();
    }

    // This is used by PaypalButtons afterPayment:
    async setUserData (user) {
        $log.debug('setUserData', user);//DEBUG
        if ( user ) {
            user = await this.checkPersonData(user);
            this.records[0].user = user;
            this.store();
        }
    }

    // showPub is asynchronous (because of verifyer).
    // showPub is ONLY called from TopPublicity, itself called from
    // Page that is, always called (except on /api/). So, showPub may
    // finish to decode the personal data stored in $person.
    async showPub () {
        let user = get(person);
        $log.debug('showPub', user);//DEBUG
        if ( ! user ) {
            return true;
        }
        if ( user.data ) {
            if ( 'showpub' in user.data ) {
                return user.data.showpub;
            } else {
                user = await this.checkPersonData(user);
            }
        } else {
            user = await this.refreshUser();
        }
        if ( user && user.data && 'showpub' in user.data ) {
            return user.data.showpub;
        }
        return true;
    }

    async checkPersonData (user) {
        $log.debug('checkPersonData1', user);//DEBUG
        if ( user.data && user.data.rawdata && user.data.cryptdata ) {
            user.data = await decodePersonData(
                user.data.rawdata, user.data.cryptdata);
            $log.debug('checkPersonData2', user, JSON.stringify(user.data));//DEBUG
            if ( user.data.end ) {
                if ( user.data.end < Date.now() ) {
                    $log.debug("checkPersonData2 remove all rights");//DEBUG
                    user.data = { showpub: true };
                } else {
                    user.data.showpub = false;
                }
            } else {
                user.data.showpub = true;
            }
        } else {
            user.data = { showpub: true };
        }
        this.setUser(user);
        return user;
    }
    
    checkRegularly () {
        const self = this;
        function clean () {
            if ( self.currentChecker ) {
                clearTimeout(self.currentChecker);
                delete self.currentChecker;
            }
        }
        function check () {
            const user = self.getUser();
            $log.debug('checkRegularly check', user, JSON.stringify(user.data));//DEBUG
            if ( user ) {
                //self.checkPersonData(user)
                self.refreshUser()
                    .then(() => self.checkRegularly())
                    .catch(() => {
                        self.clearUser();
                        clean();
                    });
            } else {
                clean();
            }
        }
        clean();
        const user = self.getUser();
        $log.debug('checkRegularly1', user);//DEBUG
        let delay = 5 * 60 * 1000; // five minutes
        if ( user && user.data && user.data.end ) {
            const dt = user.data.end - Date.now();
            if ( dt > 0 ) {
                delay = Math.min(dt+1000, delay);
            }
        }
        self.currentChecker = setTimeout(check, delay);
    }

    stillSubscribed () {
        const user = this.getUser();
        if ( user && user.data && user.data.end ) {
            const dt = user.data.end - Date.now();
            return ( dt > 0 );
        }
        return false;
    }

    async getUserSyncid (user) {
        if ( user ) {
            await this.checkPersonData(user);
            return user.data.syncid;
        }
        return undefined;
    }        
    
    badges (newbadges=undefined) {
        if ( newbadges ) {
            // set
            this.records[0].badges = newbadges;
            this.store();
        } else {
            // get
            let badges = this.records[0].badges || {};
            if ( Object.keys(badges) === 0 ) {
                this.records[0].badges = badges = defaultBadges;
                this.store();
            }
            return badges;
        }
    }
}
const defaultBadges = {
    attempts: 0,
    successes: 0
};

async function refreshPersonData () {
    const self = this;
    $log.debug('refreshPersonData1');
    try {
        const response = await fetch(`https://trinv.fr/api/person.json`, {
            method: 'GET',
            mode: 'cors',
            //credentials: 'same-origin',
            credentials: 'include',
            headers: {
                'Accept': 'application/json',
                // cf. https://stackoverflow.com/questions/73790956/cross-site-post-form-submissions-are-forbidden
                //'Content-Type': 'application/x-www-form-urlencoded'
                'Content-Type': 'application/json'
            }});
        if ( response.ok ) {
            const json = await response.json();
            $log.debug('refreshPersonData2', json);//DEBUG
            const user = create$Person(json);
            $log.debug('refreshPersonData3', user);//DEBUG
            await self.checkPersonData(user);
            person.set(user);
            return user;
        } 
    } catch (exc) {
        $log.error('refreshPersonData PROBLEM', exc.toString());
    }
}

/**
   Verify rawdata with cryptdata, then decompress rawdata into data.
   If anything went wrong return the minimal data object asking to
   show ads.

 */

async function decodePersonData (rawdata, cryptdata) {
    let data = { showpub: true };
    try {
        //let [data, signature] = rawdata.split(/[.]/);
        const verifyer = await getVerifyer();
        const ok = await verifyer(cryptdata, rawdata);
        if ( ! ok ) {
            throw new Error("decodePersonData bad signature");
        }
        data = decryptPersonData(rawdata);
        data.rawdata = rawdata;
        data.cryptdata = cryptdata;
    } catch (exc) {
        $log.error('decodePersonData PROBLEM', exc.toString());
    }
    //$log.debug('decodePersonData', data);
    return data;
}

async function importVerifyingKey () {
    let response;
    try {
        response = await fetch('/key.sign.public.jwk', {
            method: 'GET',
            mode: 'cors',
            redirect: 'follow',
            headers: {
                'Accept': 'application/json'
            }
        });
        if ( ! response.ok ) {
            const msg = "fetch failure";
            $log.error(msg);
            throw new Error(msg);
        }

        try {
            const jwk = await response.json();
            return jwk;
        } catch (exc) {
            $log.error("JSON parse failure", exc.toString());
            throw exc;
        }
    } catch (exc) {
        $log.error('importVerifyingKey Problem', exc.toString());
        throw exc;
    }
}

let theVerifyer;

async function getVerifyer () {
    if ( ! theUserconfCache ) {
        theUserconfCache = new UserconfCache();
    }
    if ( ! theVerifyer ) {
        const jwk = await importVerifyingKey();
        const alg = {
            name: 'RSASSA-PKCS1-v1_5',
            saltLength: 32,
            hash: { name: 'SHA-256' }
        };
        function decodeFromBase64 (s) {
            const uint8array = toUint8Array(s);
            //$log.debug('decodeFromBase64', uint8array);//DEBUG
            //return uint8array.buffer;
            return uint8array;
        }
        const verifyingKey = await crypto.subtle.importKey(
            'jwk', jwk, alg, false, ['verify'] );
        theVerifyer = async function verify (signature, text) {
            //$log.debug('verify base64', signature, text);//DEBUG
            const decodedSignature = decodeFromBase64(signature);
            //$log.debug('verify raw', decodedSignature);//DEBUG
            // encodedText is Uint8Array
            const encodedText = new TextEncoder().encode(text);
            //$log.debug('verify encodedText', encodedText);//DEBUG
            const ok = await crypto.subtle.verify(
                alg, verifyingKey, decodedSignature, encodedText );
            return ok;
        };
    }
    return theVerifyer;
}

/**
   decrypt compressed information (see user.mjs function formatPersonData)
 */

function decryptPersonData (rawdata) {
    const data = {};
    const items = rawdata.split(';');
    if ( items[0] !== 'v1' ) return data;
    const bools = items[1];
    if ( bools % 2 ) data.admin = true;
    if ( (bools>>1) % 2 ) data.seedpe = true;
    if ( (bools>>1) % 2 ) data.seepc = true;
    if ( (bools>>1) % 2 ) data['export'] = true;
    if ( (bools>>1) % 2 ) data.showpub = false;
    data.end = ensureNumber(items[2]);  // epoch
    data.syncid = normalizeUUID(items[3]);
    return data;
}

// end of userconfCache.mjs
