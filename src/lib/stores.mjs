// Special version for Docserver
// Excerpt from Server/src/lib/stores.mjs

import { writable, readable } from 'svelte/store';
import { Logger } from '$lib/common/logger.mjs';

export const person = writable(undefined);
export const clientConfig = writable(undefined);

// the default common logger:
export const log = writable(new Logger({
    size: 50,
    alsoconsole: true
}));

// Needed by Docserver:
export const docbase = writable('https://doc.trinv.fr');
export const hostbase = writable('https://trinv.fr');
