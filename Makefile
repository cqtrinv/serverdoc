# Time-stamp: "2025-02-12 15:18:22 queinnec"

work : common.work
clean :: cleanMakefile

common.work : build
	@echo '*** Time to commit and push'

# ####################################### static pages
# Markdown pages in ./Articles/,
#   Svelte output in ./src/routes/documentation/
#   then converted to html into ./build/
#   then finally saved in ./documentation/

src/routes/table_des_matieres/+page.svelte : \
			Makefile \
			$(shell ls Articles/*.md) \
			$(shell ls Articles/_includes/*.liquid) \
	        Articles/.eleventy.mjs \
			package.json
	cd Articles/ && ../node_modules/.bin/eleventy \
		--formats=liquid,md \
		--config=.eleventy.mjs \
		--input=. \
		--output=../src/routes/documentation/
	for d in src/routes/documentation/*/ ; do \
		mv -f $$d/index.html $$d/+page.svelte ; \
	done
#	cd src/routes/documentation/ && \
#	  mv index.html +page.svelte 
	chmod -R a-wx src/routes/documentation/*/+page.svelte
	cd src/routes/ && mv documentation/* .


# convert markdown to html directly shown by the Help component.
# Smaller HTML files without Svelte
do.static/pagedoc : \
			Makefile \
			$(shell ls PageDoc/*.md) \
			PageDoc/_includes/doclayout.liquid \
			PageDoc/.eleventy.mjs \
			package.json
	-rm -rf static/pagedoc
	cd PageDoc/ && ../node_modules/.bin/eleventy \
		--formats=liquid,md \
		--config=.eleventy.mjs \
		--input=. \
		--output=../static/pagedoc/
	rm -f static/pagedoc/index.html
#	rsync -avu static/ documentation/

update.images : update.hero
	mkdir -p static/Screens
	./node_modules/.bin/imagemin \
		../Server/Images/Screens/*.png \
		../Server/Images/Screens/*.webp \
		../Server/Images/*.webp \
			--out-dir=static/Screens/
	cd static/Screens && rm -f capture6?.png

update.hero :
	./node_modules/.bin/imagemin \
		Images/hero1.webp \
		--out-dir=static/

static/favicon.png : $(shell ls ../Logo/logo*.png )
	./node_modules/.bin/imagemin \
		../Logo/*.png \
			--out-dir=static/
	cd static/ && cp -p logo-192x192.png favicon.png
	cd static/ && cp -p testlogo-192x192.png testfavicon.png
	cd static/ && cp -p logo-180x180.png apple-touch-icon.png

update.version :
	npm version patch --no-git-tag-version
	node ./Scripts/generateVersion.js

update.components : 
	-cd src/components/ && rm -f MenuSign* BackSign* TopPublicity*
	rsync -avu \
		../Server/src/components/MenuSign.svelte \
		../Server/src/components/BackSign.svelte \
		../Server/src/components/TopPublicity.svelte \
			src/components/
	mkdir -p src/lib/client/ src/lib/common/ src/lib/client/classes/
	rsync -avu ../Server/src/lib/client/userconfCache.mjs \
			   ../Server/src/lib/client/lastCache.mjs \
			   ../Server/src/lib/client/localStorageCache.mjs \
		src/lib/client/
	rsync -avu ../Server/src/lib/client/classes/Person.mjs \
			   ../Server/src/lib/client/classes/utils.mjs \
		src/lib/client/classes/
	rsync -avu ../Server/src/lib/common/logger.mjs \
			   ../Server/src/lib/common/lib.mjs \
		src/lib/common/
	sed -ibak -e '/geoinit/d' -e '/ackground/d' src/lib/common/lib.mjs
	rm `find src -name '*bak'`

static/key.sign.public.jwk : ../Server/static/key.sign.public.jwk
	cp -fp ../Server/static/key.*.public.jwk static/

src/app.css : ../Server/src/app.css CSS/doc.css
	rsync -avu ../Server/src/app.css src/
	cat CSS/doc.css >> src/app.css

clean.static :
	if [ `find static -name '*~' | wc -l` -gt 0 ] ; \
	  then rm static/*~ ; else true ; fi

toc.order :
	grep date: Articles/*.md | sed -Ee 's@^(.*):(.*)$$@\2	\1@' | \
		sed -e 's@:date$$@@' | grep -v maintenance | sort

build : clean.src update.components update.images update.version 
build : do.static/pagedoc
build : src/routes/table_des_matieres/+page.svelte
build : src/app.css
build : static/key.sign.public.jwk
build : static/favicon.png
build : toc.order
build : clean.static
build : lint
	node ./Scripts/generateVersion.js
	npm run build
# The HTML static site in under build/

clean.src :
	-rm -rf src/routes/*
	-rm -rf src/components/*
	rsync -avu --exclude='*~' srcAddons/ src/

lint :
	npx eslint src/
	./node_modules/.bin/svelte-check

deploy : .gitlab-ci.yml
	git push

# ####################################### container
# a container is useful to test doc.trinv.fr on the
# development machine

create.container : generate.certificate
	date +%Y%m%d_%H%M%S | tr -d '\n' > docserver.tag
	npm version patch --no-git-tag-version
	-rm -rf buildlocal
	npm run build && mv build buildlocal
#	docker build --no-cache -t trinv/docserver:$$(cat docserver.tag) .
	docker build -t trinv/docserver:$$(cat docserver.tag) .
	docker tag trinv/docserver:$$(cat docserver.tag) trinv/docserver:latest

run.container :
#   docker run -it --rm trinv/docserver bash
	docker compose up -d testdocserver
	docker logs -f testdocserver

get.doc.trinv.fr.certificate :
	openssl s_client -showcerts -connect 172.30.0.47:443 |\
	sed -ne '/BEGIN CERTIFICATE/,/END CERTIFICATE/p' \
		> nginx/ssl-doc.trinv.fr.cert

generate.certificate : nginx/cert/doctrinvkey.pem
nginx/cert/doctrinvkey.pem :
	mkdir -p nginx/cert
	openssl req -x509 -newkey rsa:4096 \
		-keyout nginx/cert/doctrinvkey.pem \
		-out nginx/cert/doctrinvcert.pem \
		-sha256 -days 3650 -nodes \
		-subj "/C=FR/ST=IleDeFrance/L=Paris/O=Paracamplus/OU=TRINV/CN=doc.trinv.fr"
	chmod a+r nginx/cert/*pem

# ####################################### Once

build.now.obsolete :
# leave build/ as it is!
	rsync -avu build/ documentation/
	for f in documentation/*.html ; do \
	  sed -i.bak -e "s@[.]/_app/@/documentation/_app/@g" $$f ; \
	done
	rm -f documentation/*.bak
	-rm -f documentation/*~

deploy.test.now.obsolete :
	rsync -avu build/ static/ documentation/

deploy.now.obsolete : deploy.test
	rsync -avu --delete build/ t.trinv.fr:/var/www/t.trinv.fr/documentation/
	rsync -avu --delete build/   trinv.fr:/var/www/trinv.fr/documentation/
# NOTA: regenerating the documentation removes the build/ directory that
# is mounted by httpsrelay and then appears void in httpsrelay. Therefore
# we copy build into another directory that never disappears:
	rsync -avu --delete build/ documentation/

prepare.once :
	npm create svelte@latest Serverdoc
	cd Serverdoc/ && rsync -avu . ..
	rm -rf Serverdoc
	npm install
# set up git 
	rsync -avu ../Server/Articles .
	rsync -avu ../Server/static/Screens static/
	rsync -avu ../Server/static/favicon.* static/
	rsync -avu ../Server/static/trinv-logo.svg static/
	rsync -avu ../Server/static/logo-*.png static/
	rsync -avu ../Server/static/safari-pinned-tab.svg static/
	rsync -avu ../Server/static/apple-touch-icon.png static/
	rsync -avu ../Server/static/*.woff2 static/
	mkdir -p src/components src/lib
	rsync -avu ../Server/src/app.html src/
	rsync -avu \
		../Server/src/components/MenuSign.svelte \
		../Server/src/components/BackSign.svelte \
			src/components/
	m src/routes/+page.svelte

# To be run from docker-compose.yml
run.static.docserver :
	@echo " browse https://doc.trinv.fr"
	cd /home/node/ && npm run preview

# end of Makefile
