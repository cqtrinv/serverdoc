---
layout: pagelayout.liquid
pageTitle: Comment installer TRINV
tags: [ 'Articles' ]
date: "2022-03-04"
---

## Pourquoi installer TRINV ?

Installer TRINV permet de disposer d'une icône sur laquelle cliquer
pour lancer TRINV. Cela permet aussi à TRINV d'être une cible de
partage (comme pour les messages, courriels, navigateurs, etc.) ce qui
est très utile pour laisser l'IA de TRINV analyser une annonce
immobilière.

<img alt="TRINV comme icône" 
     class='inDocumentation'
     src="/Screens/screenshot0.jpg" />

## Comment installer TRINV ?

L'application TRINV peut être installée sur votre tablette ou votre
téléphone. Pour l'installer, votre navigateur vous proposera
(peut-être spontanément) de l'ajouter à votre écran d'accueil ou
d'installer l'application. Dans Chrome, on peut aussi passer par le
menu en haut à droite (installer TRINV). L'icône de TRINV apparaîtra
alors au sein de vos applications installées.

## Partager vers TRINV

Une fois installée, l'application TRINV peut être choisie comme cible
du partage. Si vous consultez une annonce immobilière dans un
navigateur, la partager enverra l'URL de cette annonce à TRINV qui
pourra ainsi la mémoriser ou, mieux, l'analyser pour en extraire les
principales caractéristiques.

<img alt="Partager vers TRINV" 
     class='inDocumentation'
     src="/Screens/screenshot1.jpg" />

## Rendre vos fiches persistantes

Tout ceci s'effectue anonymement et seulement dans votre navigateur
mais, pour cela, il vous faudra autoriser TRINV à stocker des
informations de façon permanente dans votre navigateur. À défaut,
votre navigateur risque d'éliminer spontanément les fiches qu'il juge
obsolètes. 

Normalement, l'application TRINV vous demandera de confirmer que vous
souhaitez que vos informations soient stockées de façon persistante
avec une question formulée de cette manière:

<img alt="Persistance en TRINV" 
     class='inDocumentation'
     style='height: 104px'
     src="/Screens/allowPersistentStorage.jpg" />

