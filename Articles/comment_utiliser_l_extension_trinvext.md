---
layout: pagelayout.liquid
pageTitle: Comment utiliser l'extension TrinvExt
tags: [ 'Articles' ]
date: "2022-03-06"
---

Cette page suppose bien sûr que l'extension `TrinvExt` est installée.
Si ce n'est pas le cas, installez-la en suivant 
<a href='{$docbase}/comment_installer_l_extension_trinvext'>
ces instructions</a>.

<div class='w3-center w3-panel'>
  <iframe width="560" height="315" 
          src="https://www.youtube.com/embed/c0Usum8VG9E"
          title="YouTube video player" 
          frameborder="0" 
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>


## Mode d'emploi

Cliquer sur l'icône de `TrinvExt`
<img src='/Screens/trinvexticon.png' alt='icone TrinvExt'> 
et choisissez le bouton « Analyser ». 

<img class='inDocumentation'
     alt="l'extension TrinvExt"
     src='/Screens/trinvext.png'>

Si vous ne voyez pas l'icône, cherchez l'icône des extensions (une
pièce de puzzle) et épinglez `TrinvExt`. Vous retrouverez `TrinvExt`
aisément la prochaine fois.

Quand vous cliquez sur « Analyser », la page courante est lue et
envoyée à l'IA de TRINV. Si l'analyse est concluante, la fiche
associée apparaîtra dans un nouvel onglet. 

En cas d'analyse défectueuse, une fiche est néanmoins créée que vous
pourrez amender, rectifier, compléter, corriger, etc.

## Exemple

Voici un [exemple en video](https://youtu.be/c0Usum8VG9E) et la même
chose textuellement: (1) l'annonce à analyser, (2) l'appel à
l'extension et (3) la fiche résultante dans un nouvel onglet:
 
<div class='photos'>
 <img class='inDocumentation'
      alt="Emploi de l'extension TrinvExt - étape 1"
      src='/Screens/trinvextuse1.png'>
 <img class='inDocumentation'
      alt="Emploi de l'extension TrinvExt - étape 2"
      src='/Screens/trinvextuse2.png'>
 <img class='inDocumentation'
      alt="Emploi de l'extension TrinvExt - étape 3"
      src='/Screens/trinvextuse4.png'>
</div>
