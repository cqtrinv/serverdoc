---
layout: faqpagelayout.liquid
pageTitle: Foire Aux Questions - FAQ
tags: [ 'Articles' ]
date: "2022-03-21"
---

<!-- ******************************************************************* -->
<article id="chercherParcelle"
         itemscope itemprop="mainEntity" 
         itemtype="https://schema.org/Question">
  <h2 itemprop="name">Comment chercher une parcelle cadastrale avec TRINV ?</h2>
  <div itemscope itemprop="acceptedAnswer" 
       itemtype="https://schema.org/Answer">
    <div itemprop="text">

Comment chercher une parcelle avec TRINV est vraiment aisé:

- rendez-vous sur [TRINV](https://trinv.fr/choix-commune)
- tapez quelques lettres pour sélectionner la commune
- indiquez la surface de la parcelle recherchée
- visualisez sur la carte toutes les parcelles ainsi sélectionnées.

<a href="{$docbase}/comment_chercher_des_parcelles_cadastrales/">
Pour en savoir plus</a>

</div></div></article>


<!-- ******************************************************************* -->
<article id="chercherUneParcelle"
         itemscope itemprop="mainEntity" 
         itemtype="https://schema.org/Question">
  <h2 itemprop="name">Je sais où se trouve la parcelle cadastrale ?</h2>
  <div itemscope itemprop="acceptedAnswer" 
       itemtype="https://schema.org/Answer">
    <div itemprop="text">

Vous savez où se trouve géographiquement le bien que vous souhaitez
investiguer mais vous ignorez l'adresse, la forme et la surface de la
parcelle. Alors,

- rendez-vous sur [TRINV](https://trinv.fr/choix-commune)
- tapez quelques lettres pour sélectionner la commune
- agrandissez la carte de la commune pour sélectionner la zone où se trouve le bien
- cliquez sur le bien, s'afficheront alors les caractéristiques du point sélectionné.

<a href="{$docbase}/comment_chercher_des_parcelles_cadastrales/">
Pour en savoir plus</a>

</div></div></article>


<!-- ******************************************************************* -->
<article id="communeAbsente"
         itemscope itemprop="mainEntity" 
         itemtype="https://schema.org/Question">
  <h2 itemprop="name">Je ne trouve pas une commune ?</h2>
  <div itemscope itemprop="acceptedAnswer" 
       itemtype="https://schema.org/Answer">
    <div itemprop="text">
    
Il peut y avoir plusieurs raison de ne pas trouver une commune.

<h3>Son nom usuel peut être différent de son nom administratif</h3>

Les communes ont souvent plusieurs graphies, toutes ne sont pas
nécessairement connues de TRINV.

Essayez une graphie avec ou sans tiret, `CONQUES-SUR-ORBIEL` et
`CONQUES SUR ORBIEL` existent mais ni `CONQUES-EN-ROUERGUE`, ni `THIN
LE MOUTIER`.

Supprimez ou ajoutez éventuellement le préfixe `le` ou `la`. Si `LE
CONQUET` existe, `CONQUET` est plus facile à taper. En revanche, `LA
HAGUE` n'est connue que comme `HAGUE`.

<h3>La commune a disparu</h3>

Dans ce cas, voyez 
<a href="{$docbase}/foire_aux_questions/#communeManquante">
cet article</a>

</div></div></article>


<!-- ******************************************************************* -->
<article id="communeManquante"
         itemscope itemprop="mainEntity" 
         itemtype="https://schema.org/Question">
  <h2 itemprop="name"> La commune n'existe plus ?</h2>
  <div itemscope itemprop="acceptedAnswer" 
       itemtype="https://schema.org/Answer">
    <div itemprop="text">

Chaque année, TRINV découvre que quelques dizaines de communes
disparaissent! Il y a des fusions de communes, des communautés de
communes, des intercommunalités, des communes déléguées, etc.

Quelques exemples:

- BINIC a fusionné avec ETABLES-SUR-MER pour donner BINIC-ETABLES-SUR-MER,
- ILE-MOLENE est maintenant rattachée à LE CONQUET qui n'a pas changé de nom.
- SAINT AMANT DE BONNIEURE, SAINT-ANGEAU et quelques autres sont devenus VAL DE BONNIEURE qui n'existait pas auparavant.

Si vous cherchez une commune disparue, il vous faut chercher dans la
commune englobante, le plus souvent une commune proche. Pour ce faire,
je consulte en général <a href='https://fr.wikipedia.org/'>
Wikipédia</a> qui indique souvent quand
la commune a disparu et ce qu'elle est devenue.

Enfin il y a quelques cas étranges (comme `THIN-LE-MOUTIER`) pour
lesquels, la commune semble exister mais sans cadastre associé ou
alors je ne le trouve pas!

</div></div></article>


<!-- ******************************************************************* -->
<article id="plageSurfaces"
         itemscope itemprop="mainEntity" 
         itemtype="https://schema.org/Question">
  <h2 itemprop="name">Comment chercher des parcelles cadastrales avec une plage de surfaces ?</h2>
  <div itemscope itemprop="acceptedAnswer" 
       itemtype="https://schema.org/Answer">
    <div itemprop="text">

Pour chercher, avec TRINV, les parcelles de surface comprise entre 100
et 105 m² indiquez `100-105` tout simplement. Attention à ne pas
demander trop de parcelles, cela prendra beaucoup de temps et vous ne
pourrez probablement pas explorer le tout. Faites plutôt des demandes
progressives comme `100-102`, `102-105` ...

De plus, dû à des limitations sur le nombre de requêtes que l'on peut
envoyer aux serveurs du GeoPortail, le nombre de parcelles résultant
d'une requête est limité à quelques dizaines.

</div></div></article>



<!-- ******************************************************************* -->
<article id="parcellesAbsentes"
         itemscope itemprop="mainEntity" 
         itemtype="https://schema.org/Question">
  <h2 itemprop="name"> Pourquoi toutes les parcelles cadastrales ne sont pas déjà en ligne ?</h2>
  <div itemscope itemprop="acceptedAnswer" 
       itemtype="https://schema.org/Answer">
    <div itemprop="text">

Certaines communes n'ont que quelques centaines de parcelles quand
d'autres en ont plus cent mille. Il faut donc du temps pour
les incorporer dans la base de données de TRINV. De plus, il y a plus
de trente mille communes en France et toutes ne font pas l'objet de
recherches par les internautes. Les fichiers des parcelles cadastrales
ne sont donc chargés qu'à la demande.

Prenez donc votre mal en patience mais rappelez-vous qu'une fois
incorporées, les données sont valables jusqu'à la prochaine version du
cadastre qui est mis à jour chaque trimestre.

</div></div></article>


<!-- ******************************************************************* -->
<article id="parcellesSansAdresse"
         itemscope itemprop="mainEntity" 
         itemtype="https://schema.org/Question">
  <h2 itemprop="name"> Pourquoi certaines adresses de parcelles cadastrales sont inconnues ?</h2>
  <div itemscope itemprop="acceptedAnswer" 
       itemtype="https://schema.org/Answer">
    <div itemprop="text">

Les coordonnées du centre d'une parcelle servent d'indice pour résoudre
l'adresse correspondante. Les sites gouvernementaux peuvent donner
plusieurs réponses, chacune accompagnée de la distance avec le centre
de la parcelle. TRINV élimine toutes les adresses à plus de 100
mètres. Ainsi certaines parcelles sont-elle dépourvues d'adresse.

Périodiquement, TRINV rafraîchit sa base d'adresses résolues qui,
depuis la loi 3DS impose à toutes les communes de dénommer et
numéroter les voies communales. Ainsi certaines parcelles
acquièrent-elles de nouvelles adresses.

</div></div></article>


<!-- ******************************************************************* -->
<article id="applications"
         itemscope itemprop="mainEntity" 
         itemtype="https://schema.org/Question">
  <h2 itemprop="name"> Quelle application pour trouver une parcelle cadastrale ?</h2>
  <div itemscope itemprop="acceptedAnswer" 
       itemtype="https://schema.org/Answer">
    <div itemprop="text">

Nul besoin d'installer une application sur Android ou IOS pour trouver
une parcelle cadastrale. Il suffit d'accéder au <a
href='https://www.geoportail.gouv.fr/'>GeoPortail</a> officiel,
d'indiquer la commune désirée puis naviguer sur la carte présentée. 

Par ailleurs, existent d'autes sites, autres que <a
href='https://trinv.fr/'>TRINV</a> comme, par exemple, 
le <a href='https://public.geofoncier.fr/'>site des experts géomètres</a>,

</div></div></article>


<div>
 <h2>Rien trouvé ?</h2>
 <p>
   Vous n'avez pas trouvé de réponse à votre interrogation ?
   <a href='{$docbase}/contacter_trinv/'>Contactez-nous</a>
   et nous enrichirons cette FAQ.
 </p>
</div>
