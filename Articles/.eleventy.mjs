// Eleventy configuration

import fs from 'fs';

/* 
    With eleventy 3.0, this file may be a normal module.

    Articles are by default sorted by the 'date' field.
*/

export default function (eleventyConfig) {
    // API is available in `eleventyConfig` argument
    //console.log('eleventyConfig loaded', eleventyConfig);

    eleventyConfig.addCollection("AllArticles", function(collectionApi) {
        for ( const post of collectionApi.getAll() ) {
            //console.log(post.data);
            post.data.pageTitle = computeTitle(post.data.pageTitle);
            post.page.canonicalURL = computeCanonicalURL(post);
            post.page.version = computeVersion();
            //console.log(post);
        }
        //console.log(collectionApi);
        const AllArticles = collectionApi.getAll().filter(function (post) {
            if ( Array.isArray(post.data.tags) ) {
                const tags = post.data.tags;
                if ( tags.indexOf('Articles') >= 0 ) {
                    return true;
                }
            }
            return false;
        });
        //console.log('all', collectionApi.getAll().length); // 21
        //console.log('AllArticles', AllArticles.length);    // 19
        return AllArticles;
    });
    
    return {
        // your normal config options
        markdownTemplateEngine: "liquid"
    };
};

function computeTitle (pathname) {
    //console.debug('computeTitle', `"${pathname}"`);//DEBUG
    if ( pathname.match(/^(comment|que|quoi) .*[^?]\s*$/i) ) {
        pathname += ' ?';
    }
    pathname = pathname.replace(/trinv/ig, 'TRINV');
    pathname = pathname.charAt(0).toUpperCase() +
        pathname.slice(1);
    return pathname;
}

function computeCanonicalURL (post) {
    // assume post.page === post.data.page
    //console.debug('computeCanonicalURL', post.url);
    let url = post.url;
    url = url.replace(/^(.+[^\/])\/+$/, '$1') + '/';
    url = url.replace(/^\/+$/, '/');
    // Always a single / at the end of the URL!
    //console.log('computeCanonicalURL', url);//DEBUG
    return url;
}

function computeVersion () {
    const pkg = JSON.parse(fs.readFileSync('../package.json'));
    const version = pkg.version;
    return version;
}
