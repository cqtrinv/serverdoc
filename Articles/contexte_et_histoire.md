---
layout: pagelayout.liquid
pageTitle: Contexte et histoire
tags: [ 'Articles' ]
date: "2022-03-20"
---

<style>
img {
  height: 40ex;
  border: solid 1px #910E3A;
}
</style>

## Pourquoi TRINV ?

J'ai commencé, à l'été 2018, à chercher une maison en Bretagne au bord
de la mer. À l'été 2024, je cherche toujours! Mais pendant ces
longs mois, j'ai amélioré mes méthodes de recherche, méthodes que je
partage ici et dont TRINV est une des composantes importantes.

Au début, une feuille de tableur me permettait d'engranger les
annonces immobilières, leur URL, les caractéristiques des maisons, les
coordonnées des agences et des agents, mes notes personnelles
consécutives à mes prises de renseignement, etc.

Au bout d'une centaine de lignes, cette feuille est devenu ingérable.

## ReU

J'ai donc résolu d'écrire une application Web, nommée ReU, pour gérer
toutes ces informations. Peu à peu, je l'ai enrichie pour me faciliter
aussi la géolocalisation de maisons à partir des descriptions qu'en
faisaient les agences. Grâce à Google Map, Street View, GeoPortail et le
cadastre j'ai, en général, pu localiser la moitié des maisons en
vente, m'évitant ainsi (puisque j'habite Paris et que je cherche en
Bretagne) de me déplacer pour apprécier l'environnement au sein duquel
se trouvent ces maisons.

Cette application nécessiterait d'être totalement réécrite pour être
diffusable et utilisable par d'autres que moi mais en attendant une
brique est publique et disponible: TRINV.

## TRINV

TRINV est un cadastre inversé. Étant donné une commune et une surface
de parcelles, TRINV affiche, sur une carte, les parcelles concernées.
Voyez ici <a href="{$docbase}/comment_chercher_des_parcelles_cadastrales/">
comment chercher des parcelles cadastrales</a>.

## Futur

TRINV est voué à se développer pour vous aider dans la recherche d'une
maison ou d'un terrain. Vous pouvez d'ores et déjà consulter mes 
<a href="{$docbase}/comment_chercher_une_maison/">
conseils pour cette recherche</a>.

Enfin, consultez les dernières 
<a href="{$docbase}/quoi_de_neuf_dans_trinv/">nouveautés</a> 
ou <a href="{$docbase}/contacter_trinv/">
faites-moi part de vos suggestions</a>.
