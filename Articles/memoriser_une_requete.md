---
layout: pagelayout.liquid
pageTitle: Mémoriser une requête
tags: [ 'Articles' ]
date: "2022-03-07"
---

Bien que les agrégateurs d'annonces immobilières soient assez
performants, il arrive qu'ils loupent une nouvelle annonce qu'ils ne
verront que quelques jours plus tard. Aussi est-il important de
souvent arpenter ces sites afin de dénicher la nouvelle annonce au
plus vite.

## Mémoriser une requête

Certes de nombreux sites vous proposent de vous alerter lorsqu'une
nouvelle annonce apparaît mais, d'une part, cela engendre un afflux de
courriels tellement répétitifs qu'on ne voit plus vraiment ce qui est
nouveau (sans parler des <a href="/comment_chercher_une_maison/#fausseNouveaute">fausses nouveautés</a>) et, d'autre part,
il n'est pas garanti que ces sites vous préviennent rapidement (ce
peut être tous les jours ou toutes les semaines).

Aussi est-il utile de conserver dans ses favoris les URLs menant à des
informations utiles et intéressantes. Par exemple, 

```
https://www.nestoria.fr/erquy/immobilier/vente
https://www.seloger.com/list.htm?&idtypebien=2&idtt=2,5&naturebien=1,2,4&ci=140117
```

La première URL permet d'afficher toutes les annonces immobilières
d'Erquy connues de Nestoria. La seconde permet d'afficher les
annonces immobilières de Cabourg connues de SeLoger.

Certes il faut un peu de doigté pour comprendre le schéma d'URL du
site que vous souhaitez mémoriser mais cette astuce fonctionne sur
nombre d'entre eux. 

Vous pouvez également utiliser le système de signets de votre navigateur.
Toutefois, le plus grand avantage de TRINV est que vous pourrez aisément
<a href="/analyser_une_liste_d_annonces/">créer ou retrouver vos fiches</a> 
correspondant aux annonces affichées.
