---
layout: pagelayout.liquid
pageTitle: Comment organiser ses recherches
tags: [ 'Articles' ]
date: "2022-03-06"
---

Suis-je un bon modèle d'organisation ?

Je suis à la recherche d'une maison en Bretagne depuis de longues
années. J'ai d'abord cherché dans une petite zone du Morbihan puis
j'ai étendu ma recherche au littoral breton entier, enfin, récemment,
je me suis restreint aux environs de Carnac. Depuis 2018 (cf. <a
href='/contexte_et_histoire/'>contexte et histoire</a>) les prix ont
doublé ce qu'ont loin d'avoir fait mes économies! 

Peut-être suis-je trop à la recherche de la maison parfaite que je
ne trouverai jamais.

## Ma méthode

Je suis abonné à beaucoup de sites et je reçois tous les jours une
dizaine de résultats d'alertes. 

Je consulte toutes les semaines les aggrégateurs (j'ai longtemps
apprécié Nestoria mais, aujourd'hui, je préfère MoteurImmo). 

J'archive toutes les annonces nouvelles. Pour chacune des annonces
provenant d'un aggrégateur, je cherche l'annonce originale de l'agence
concernée et je l'archive aussi. Je fusionne toutes les fiches qui
parlent du même bien et je complète les caractéristiques manquantes.

C'est là que l'on s'aperçoit que les prix varient, que les
caractéristiques sont légèrement différentes, que certaines photos
montrent des détails intéressants qu'on ne voit pas ailleurs.

Munis de toutes ces informations, je cherche à déterminer l'adresse.
J'arrive aujourd'hui à localiser plus des trois quarts des annonces
dans mon petit secteur de recherches que je finis par bien connaître.

En parallèle, si le bien me semble intéressant, je choisis l'agence
qui me semble la plus sympathique et la joins au téléphone pour poser
<a href='/comment_chercher_une_maison/#Critères'>quelques
questions</a>.

## Miscellanées

Juste quelques remarques déduites de mes recherches.

- Il y a en général une ou deux agences immobilières dominantes dans
  une région.
- Certaines agences ne publient que sur un ou deux aggrégateurs et
  n'ont pas de site propre.
- Si le texte de la description est, en général, le même sur les
  différents sites où l'annonce est publiée, les détails (surface,
  terrain, nombre de salles de bain, etc.) diffèrent souvent.
- Ne tardez pas à vous renseigner, les beaux biens au bon prix
  disparaissent en quelques jours.
- Inversement, certains biens restent sur le marché depuis de nombreux
  mois. En général, soit le prix est trop élevé, soit le voisinage
  n'est pas optimal.



