---
layout: pagelayout.liquid
pageTitle: Comment chercher des parcelles cadastrales
tags: [ 'Articles' ]
date: "2022-03-02"
---

<div itemscope itemtype='http://schema.org/HowTo'>
<div itemprop="description" 
     value="Comment chercher des parcelles cadastrales" ></div>
<div itemprop="author" itemscope itemtype="https://schema.org/Person">
  <div itemprop="name" value="Christian Queinnec" ></div>
</div>
<!-- div itemprop="supply" value="" / -->
<div itemprop="tool" value="Internet access" >&nbsp;</div>
<div itemprop="totalTime" value="P1M" >&nbsp;</div>
<div itemprop="tool" itemscope itemtype="https://schema.org/HowToTool">
    <div itemprop='url' value="https://trinv.fr/" >&nbsp;</div>
</div>
<div itemprop="datePublished" value="{{ date }}" ></div>

Chercher une parcelle cadastrale est très simple: vous indiquez d'abord
(1) la commune puis (2) la surface. Vous n'avez alors plus qu'à trier 
parmi les possibilités. 

Voici une suite de captures d'écrans illustrant la recherche de
parcelles cadastrales avec TRINV à partir de la page initiale:

<img itemprop="image" alt="page initiale" 
     class='inDocumentation'
     src="/Screens/img0.png" />

<!-- ************************** step **************** -->
<div itemscope itemprop="step" itemtype="http://schema.org/HowToStep">
<h2 itemprop="name">Sélectionner une commune</h2>
<div itemprop="url" value="https://trinv.fr/choix-commune">&nbsp;</div>
<div itemprop="text">

Commençons donc et tapons quelques lettres du nom de la commune qui
nous intéresse (on peut aussi indiquer un code postal) puis cliquons
sur le nom de la commune.

<img itemprop="image" alt="choix de la commune"
     class='inDocumentation'
     src="/Screens/img1.png" />
     
</div>
</div>

<!-- ************************** step **************** -->
<div itemscope itemprop="step" itemtype="http://schema.org/HowToStep">
<h2 itemprop="name">Indiquer la surface</h2>
<div itemprop="url" value="https://trinv.fr/choix-surface?inseeid=29292&commune=TREGUENNEC">&nbsp;</div>
<div itemprop="text">

Cherchons les parcelles de 500 m<sup>2</sup> dans Treguennec et
cliquons sur le bouton « Chercher ».

<img itemprop="image" alt="choix de la surface"
     class='inDocumentation'
     src="/Screens/img2.png" />
     
Vous pouvez également spécifier un intervalle (pas trop grand!) 
pour les surfaces. Indiquez, par exemple, 500..502 pour chercher les 
parcelles de surface 500, 501 et 502 m<sup>2</sup>.

Apparaît aussi la carte de la commune sélectionnée. Cette carte peut
aussi être visualiseée en vue satellitaire à l'aide du bouton M. Vous
pouvez également cliquer sur la carte pour accéder au cadastre ou à
une vue satellitaire détaillée.

<img itemprop="image" alt="autres vues détaillées"
     class='inDocumentation'
     src="/Screens/popup2.png" />
     
</div>
</div>

<!-- ************************** step **************** -->
<div itemscope itemprop="step" itemtype="http://schema.org/HowToStep">
<h2 itemprop="name">Affichage des parcelles</h2>
<div itemprop="url" value="https://trinv.fr/carte-parcelles?area=500&inseeid=29292&commune=TREGUENNEC">&nbsp;</div>
<div itemprop="text">
 
Sous la carte (procurée par
[GeoPortail](https://www.geoportail.gouv.fr/)) visualisant les
parcelles, s'affiche la liste des parcelles (avec leur identifiant
officiel) et leur adresse postale. Cette adresse provient d'une
géolocalisation inverse (procurée par
[GeoPortail](https://www.geoportail.gouv.fr/)).

<img itemprop="image" alt="Affichage des parcelles"
     class='inDocumentation'
     src="/Screens/img3.png" />
    
Lorsque le cadastre d'une commune n'est pas encore connu de TRINV,
aller le chercher et le traiter prend entre 1 et 5 minutes suivant la
volumétrie du cadastre. Si les plus petites communes aujourd'hui
connues de TRINV n'ont que moins de 100 parcelles, les plus grandes
(comme par exemple, Toulouse) en ont environ 100 000! En revanche, si
le cadastre est déjà connu, les parcelles sont assez rapidement
affichées avec les adresses associées.

Apparaît aussi la carte de la commune sélectionnée. Cette carte peut
aussi être visualiseée en vue satellitaire à l'aide du bouton M. Vous
pouvez également cliquer sur la carte pour accéder au cadastre ou à
une vue satellitaire détaillée.

</div>
</div>

<!-- ************************** step **************** -->
<div itemscope itemprop="step" itemtype="http://schema.org/HowToStep">
<h2 itemprop="name">Cacher des parcelles</h2>
<div itemprop="url" value="https://trinv.fr/carte-parcelles?area=500&inseeid=29292&commune=TREGUENNEC">&nbsp;</div>
<div itemprop="text">

Cliquer sur une des parcelles révèle un petit menu affichant l'adresse
de la parcelle et trois boutons. Si la parcelle ne vous intéresse pas,
cliquez sur « NON » pour la rendre invisible et la supprimer de la
liste des parcelles.

<img itemprop="image" alt="Affichage des parcelles"
     class='inDocumentation'
     src="/Screens/img4.png" />
     
Une autre façon de supprimer les parcelles inintéressantes est de zoomer
sur la zone appropriée et de cliquer le bouton représentant un carré: 
il supprimera de la liste des parcelles toutes celles qui sont hors de vue.

<img itemprop="image" alt="Affichage des parcelles"
     class='inDocumentation'
     src="/Screens/msquare-annotated.svg" />
 
</div>
</div>

<!-- ************************** step **************** -->
<div itemscope itemprop="step" itemtype="http://schema.org/HowToStep">
<h2 itemprop="name">Parcelle cadastrale</h2>
<div itemprop="url" value="/Screens/img5.png">&nbsp;</div>
<div itemprop="text">

Cliquez sur « Cadastre » pour afficher les limites cadastrales
(fournies par [GeoPortail](https://www.geoportail.gouv.fr/)) 

<img itemprop="image" alt="Affichage d'une parcelle cadastrale"
     class='inDocumentation'
     src="/Screens/img5.png" />
     
 
</div>
</div>

<!-- ************************** step **************** -->
<div itemscope itemprop="step" itemtype="http://schema.org/HowToStep">
<h2 itemprop="name">Vue aérienne ou StreetView</h2>
<div itemprop="url" value="/Screens/img6.png">&nbsp;</div>
<div itemprop="text">

Cliquez sur « Voir » pour afficher une photo aérienne plus précise
fournie par Google ce qui permet, le cas échéant d'utiliser StreetView.

<img itemprop="image" alt="Carte aérienne"
     class='inDocumentation'
     src="/Screens/img6.png" />
<img itemprop="image" alt="Carte aérienne"
     class='inDocumentation'
     src="/Screens/img7.png" />

Vous pouvez également utiliser le bouton M pour passer de la carte à la
vue aérienne ou inversement.

<img itemprop="image" alt="Autre vue aérienne"
     class='inDocumentation'
     src="/Screens/vam.png" />

</div>
</div>

<p>
Bonnes recherches!
</p>
 
</div>
