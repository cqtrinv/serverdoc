---
layout: pagelayout.liquid
pageTitle: Comment installer l'extension TrinvExt
tags: [ 'Articles' ]
date: "2022-03-05"
---

## Qu'est-ce qu'une extension ?

Une extension est un logiciel que vous ajoutez à votre navigateur afin
de lui conférer de nouveaux comportements. `TrinvExt` est une
extension à Chrome qui simplifie la mise en oeuvre de TRINV pour la
mémorisation d'annonces immobilières.

Attention: actuellement, l'extension n'est installable que sur
ordinateur. Les navigateurs sur téléphones n'acceptent pas
d'extension.

TRINV mémorise une annonce immobilière en extrayant ses principales
caractéristiques. Pour cela, l'IA de TRINV a besoin de lire puis
d'analyser la page de cette annonce ce que permet `TrinvExt`
sans autre effort qu'un simple et seul clic.

Comme ajouter une extension à son navigateur est une opération qui
peut comporter des risques, le
<a href="https://gitlab.com/cqtrinv/chromeextension">
code de cette extension</a>
est public. Cette extension ne modifie rien dans la page courante et
se contente de la lire puis de l'envoyer à TRINV.

## Installation

Pour installer `TrinvExt`, il faut chercher dans le 
<a href="https://chrome.google.com/webstore/detail/trinvext/ekemgbfjoofpddmmoocaocklahbojcll">
magasin des extensions Chrome</a>

Actuellement, cette extension fonctionne sur plusieurs navigateurs
listés ci-dessous. Il se pourrait qu'elle fonctionne aussi pour
d'autres navigateurs. En revanche, les navigateurs sur tablette et
téléphones n'acceptent pas d'extension.

### Chrome

Installer à partir du
[Chrome Web Store](https://chrome.google.com/webstore), chercher
`TrinvExt` puis cliquer sur la ligne afin d'afficher les détails à
propos de `TrinvExt` (vous pouvez aussi arriver là avec le
[lien direct[(https://chrome.google.com/webstore/detail/trinvext/ekemgbfjoofpddmmoocaocklahbojcll)) et, enfin, cliquer sur le bouton « Disponible sur Chrome ».

### Brave

Pour Brave, même procédure. Le 
[Chrome Web Store](https://chrome.google.com/webstore)
est un réservoir d'extensions possibles. Vous aurez juste à accepter que 
l'extension n'a pas été vérifiée par Brave (elle le fut avec Google). 

<img class='inDocumentation'
     alt="l'extension TrinvExt pour Brave"
     src='/Screens/trinvext-brave.png'>

### Edge

Pour Edge, même procédure. Le
[Chrome Web Store](https://chrome.google.com/webstore) est un
réservoir d'extensions possibles. Vous aurez juste à accepter que
l'extension n'a pas été vérifiée par Edge (elle le fut avec Google).
N'oubliez pas d'épingler l'extension.

<img class='inDocumentation'
     alt="l'extension TrinvExt pour Edge"
     src='/Screens/trinvext-edge.png'>

### Opera

Pour Opera, même procédure. Le
[Chrome Web Store](https://chrome.google.com/webstore) est un
réservoir d'extensions possibles. Vous aurez juste à accepter que
l'extension n'a pas été vérifiée par Opera (elle le fut avec
Google).N'oubliez pas d'épingler l'extension.

<img class='inDocumentation'
     alt="l'extension TrinvExt pour Opera"
     src='/Screens/trinvext-opera.png'>

### Firefox

Il faut avoir un Firefox récent et aller chercher l'extension `TrinvExt`
sur le [magasin d'extensions de Firefox](https://addons.mozilla.org/en-US/firefox/addon/trinvext/). 

### Safari

L'extension n'est pas disponible pour Safari, veuillez utiliser un des
navigateurs mentionnés ci-dessus.

## Explication du code

Le fichier `package.json` indique les fichiers présents dans l'extension. 
Il contient ce qui est nécessaire pour la regénération de l'extension.

Le seul fichier exécutable et vraiment intéressant est `popup.js` qui
capture le code de la page contenant l'annonce et l'envoie à l'IA de
TRINV pour analyse. 

Le fichier `iAmHere.js` ne sert à TRINV qu'à repérer si l'extension
est installée ou pas.

