---
layout: pagelayout.liquid
pageTitle: Comment utiliser TRINV
tags: [ 'Articles' ]
article: 0.0
---

<style>
a {
    color:  #910E3A;
}
</style>

- Vous connaissez la commune et la superficie: 
  choisissez la <a href='{$hostbase}/choix-commune'>commune</a> puis
  indiquez la <a href='{$hostbase}/choix-surface'>surface</a>
  
- Vous pensez savoir dans quel coin elle est:
  choisissez la <a href='{$hostbase}/choix-commune'>commune</a> puis
  zoomez, choisissez la bonne cartographie (plan ou cadastre ou photo satellitaire)
  et cliquez sur la carte de la commune

- Vous pensez qu'un DPE vous aidera: 
  choisissez alors la <a href='{$hostbase}/choix-commune'>commune</a> puis
  demandez la <a href='{$hostbase}/carte-dpe'>carte des DPE</a>

- Vous connaissez la référence de la parcelle cadastrale:
  indiquez-la <a href='{$hostbase}/parcelle'>référence</a>

## Pour en savoir plus, beaucoup plus ...

Survolez les <a href='/table_des_matieres'>différents articles</a>
pour encore mieux utiliser TRINV pour vos recherches.
