---
layout: pagelayout.liquid
pageTitle: Analyser une liste d'annonces
tags: [ 'Articles' ]
date: "2022-03-07"
---

Grâce à cette fonctionnalité, vous pouvez surveiller les annonces d'un
site aggrégateur et, aisément:

- visualiser les annonces nouvelles et les mémoriser en des fiches
- ignorer certaines annonces car inintéressantes
- ou bien chercher si une annonce n'est que la réédition d'une annonce que vous connaissiez déjà!

## Mode d'emploi

Il est possible, avec l'<a href='{$docbase}/comment_installer_l_extension_trinvext/'>extension TRINVext</a>, d'analyser certaines pages
d'aggrégateurs listant de nombreuses annonces. Le résultat est une
nouvelle page listant toutes les annonces capturées avec leurs
principales caractéristiques.

### Analyse

Voici, par exemple, une liste d'annonces fournies par un aggrégateur:

<img itemprop="image" alt="Voir plusieurs annonces" 
     class='inDocumentation'
     src="/Screens/multiple0.webp" />

Après avoir demandé à TRINVext d'analyser cette page, on obtient
la liste des grandes caractéristiques de ces annonces:

<img itemprop="image" alt="Voir plusieurs annonces analysées" 
     class='inDocumentation'
     src="/Screens/multiple1.webp" />
     
Un code couleur est associé aux annonces analysées: les annonces
peuvent être déjà connues (bordure continue) ou inconnues (bordure
pointillée) ou avoir été ignorées (sur fond gris). Cliquer sur une
annonce révèle un menu qui permet de l'ignorer ou de la convertir en
une fiche ou encore de la rapprocher d'une fiche déjà connue et ainsi
de les fusionner.

<img itemprop="image" alt="Menu d'annonce" 
     class='inDocumentation'
     src="/Screens/multiple2.webp" />
     
Cliquer sur le titre de l'annonce permet d'afficher l'annonce
originale dans un autre onglet. Il n'est pas possible de modifier les
caractéristiques de l'annonce, une fiche doit d'abord être créée.
     
### Ignorer une annonce

Ignorer une annonce, l'affiche en gris. Lorsque vous réanalyserez les
annonces de l'aggrégateur, les annonces ignorées resteront ignorées
vous permettant ainsi de vous concentrer sur ce qui est nouveau.
   
Le résultat de votre analyse est mémorisé et sera pris en compte la
prochaine fois: une annonce ignorée restera ignorée, une annonce
connue sera encore connue. Si d'aventure une annonce ignorée vous
intéresse, vous pouvez la reconnaître afin qu'elle ne soit plus ignorée.

<!-- img itemprop="image" alt="Annonce ignorée" 
     class='inDocumentation'
     src="/Screens/multiple3.webp" / -->
     
### Rapprocher une annonce
     
Vous pouvez également rapprocher l'annonce des fiches que vous avez
accumulées afin de chercher une fiche ayant une description similaire.
Les fiches candidates sont alors affichées et vous pouvez choisir
d'identifier, c'est-à-dire fusionner, cette nouvelle annonce avec une
annonce déjà connue.

<img itemprop="image" alt="Rapprochement d'annonce" 
     class='inDocumentation'
     src="/Screens/multiple4.webp" />
     
Notez que les caractéristiques qui diffèrent sont barrées
horizontalement. À vous d'apprécier la compatibilité du rapprochement
proposé.

     
### Créer fiche

Si l'annonce vous intéresse, vous pouvez la convertir en une fiche
puis afficher cette fiche afin de la compléter. Une fois créée,
l'annonce est bordée avec un trait continu et le menu ne permet que de
voir la fiche associée.

<img itemprop="image" alt="Annonce et fiche" 
     class='inDocumentation'
     src="/Screens/multiple5.webp" />
     

### Remarques
     
Actuellement, cette fonctionnalité n'est disponible que sur quelques
agrégateurs: MoteurImmo (limité à une dizaine d'annonces), Nestoria et
SeLoger sont possibles.
