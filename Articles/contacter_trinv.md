---
layout: pagelayout.liquid
pageTitle: Contacter TRINV
tags: [ 'Articles' ]
date: "2022-03-31"
---

## Pensez à nous!

- Vous avez des remarques ou des souhaits ? 
- Vous avez trouvé une maison ou un terrain grâce à TRINV ? 
- Vous pensez à de nouvelles fonctionnalités qui vous aiderait dans votre recherche d'un bien ?

Signalez-le nous par courriel à
[contact@trinv.fr](mailto:contact@trinv.fr)
ou donnez-nous [plein d'étoiles](https://g.page/r/CXOMgYr69by2EAI/review) 
ou, tout simplement, [votre avis](https://g.page/r/CXOMgYr69by2EBM/review).

Vous pouvez également nous suivre sur
- [Facebook](https://www.facebook.com/search/posts?q=trinv%20cadastre)
- [Google](https://trinv.business.site/)
