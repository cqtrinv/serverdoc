---
layout: pagelayout.liquid
pageTitle: Mémoriser une annonce immobilière
tags: [ 'Articles' ]
date: "2022-03-02"
---

Mémoriser les annonces immobilières vous intéressant est plus
qu'utile! Elles vous permettront de voir les prix ou les durées de
mise en vente évoluer, de comparer des biens comparables, d'accumuler
des photos d'un même bien affiché par plusieurs agences. Pour toutes
ces raisons et, notamment, parce que les sites aggrégateurs font
régulièrement disparaître les annonces, il faut donc les mémoriser
pour soi.

## La meilleure solution

Pour les mémoriser, la meilleure solution est d'utiliser l'extension
TRINV (astucieusement nommée TRINVext): un petit logiciel
complémentaire à installer dans votre navigateur. Une fois installé,
vous affichez l'annonce vous intéressant, vous cliquez sur l'icône de
l'extension et vous laissez TRINV condenser l'annonce en une fiche.

Pour en savoir plus:

- <a href="{$docbase}/comment_installer_l_extension_trinvext/">installer l'extension TRINVext</a>
- <a href="{$docbase}/comment_utiliser_l_extension_trinvext/">utiliser l'extension TRINVext</a>

## Autre possibilité

Malheureusement, tous les sites d'annonces ne sont pas analysables!
Si TRINVext échoue, TRINV vous proposera une seconde solution.
Saisissez l'URL de l'annonce dans le formulaire qui vous sera 
proposé et demandez son analyse.

## Dernier ressort

Malheureusement, là encore, l'analyse peut échouer souvent à cause
d'une captcha. On peut alors se tourner vers cette méthode qui demande
un peu de dextérité mais rien de sorcier! Il s'agit de capturer 
le code HTML de la page et de l'envoyer à TRINV. 

La procédure entière est ici détaillée:

- <a href="/comment_capturer_l_html_d_une_annonce/">Comment aider TRINVext</a>

<img itemprop="image" alt="échec de TRINVext" 
     class='inDocumentation'
     src="/Screens/trinvextFailure.webp" />

## Ultime solution

Dans tous les cas, une fiche a été créée que vous pouvez compléter 
avec les données de l'annonce et ainsi garder une trace durable
de cette annonce! Si vous visitez ce bien, vous pourrez toujours
y ajouter vos propres photos.



