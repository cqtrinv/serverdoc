---
layout: pagelayout.liquid
pageTitle: Arrêt de TRINV pour maintenance
tags: [ 'internal' ]
date: "2024-09-30"
---

Une révision importante de TRINV est actuellement en cours
nécessitant d'interrompre le service. 
TRINV devrait être à nouveau disponible dans 2 heures.


