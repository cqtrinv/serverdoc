---
layout: pagelayout.liquid
pageTitle: À propos des DPE
tags: [ 'Articles' ]
date: "2025-01-06"
---

Un DPE ou diagnostic de performance énergétique est un document
obligatoire pour les ventes de biens. Il qualifie les biens
immobiliers, à l'aide de deux lettres entre A et G, suivant leur
performance. Ces deux lettres doivent être apparentes sur les annonces
immobilières. 

L'[Observatoire DPE-Audit](https://observatoire-dpe-audit.ademe.fr/accueil) 
de l'ADEME recense tous les DPE. Et comme les DPE doivent être
réalisés avant mise en vente, ils permettent de connaître les biens en
vente. Il faut toutefois relativiser leur importance:

## les DPE sont valables 10 ans

Un bien peut donc être revendu sans qu'un nouveau DPE soit nécessaire.

## DPE et biens à vendre

Les DPE sont aussi nécessaires pour les locations. Les DPE ne sont donc pas tous
associés à des biens à vendre.

## Adresses approximatives

Les adresses renseignées sont souvent approximatives. Le bien inspecté
peut se situer dans une importante résidence sans plus de détails. Le
bien peut être sur une voie de quelques kilomètres de long sans que
son numéro dans la voie soit indiqué.

On comprend alors que la géolocalisation inverse réalisée par l'ADEME
à partir d'une adresse potentiellement approximative conduise à des
résultats farfelus!
  
## Délai d'archivage

Les DPE ne sont pas toujours immédiatement archivés sur l'ADEME.

## Variabilité des DPE

Lorsqu'un mauvais DPE est obtenu, le propriétaire peut en faire
réaliser d'autres plus favorables. Les annonces ne sont pas forcément
mises à jour ce qui rend plus difficile l'appariement d'une fiche et
d'un DPE.

# Conclusions

Les DPE peuvent néanmoins être très utiles!
C'est une information à ne pas négliger lors de vos recherches.
