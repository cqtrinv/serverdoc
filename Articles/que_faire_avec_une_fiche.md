---
layout: pagelayout.liquid
pageTitle: Que faire avec une fiche
tags: [ 'Articles' ]
date: "2022-03-07"
---

<style>
.grey {
  background-color: #eee;
  padding: 1rem;
}
</style>

## Vos favoris 

Choisir « Favoris » dans le menu général permet de lister toutes les
annonces ou simples URLs que vous avez mémorisées dans votre
navigateur. C'est l'analogue des signets ou favoris que l'on trouve
dans les navigateurs sauf que vous pouvez agir sur ces données à
l'aide du menu contextuel (trois points alignés verticalement).

Si vous n'avez aucun favori, quelques fiches sont automatiquement
créées menant vers des sites utiles pour la recherche de biens:
cadastre, geoportail et demandes de valeurs foncières. Vous pouvez
ajouter vos propres URL avec le bouton `+` en haut à droite.

Ces fiches, qui viennent d'être créées, sont atypiques car les favoris
sont plutôt là pour mémoriser des annonces immobilières mais elles
constituent des exemples de fiches (que vous pouvez bien sûr
supprimer).

## Que mémoriser ?

Vous pouvez certes mémoriser des annonces. Comme les annonces
disparaissent régulièrement et assez rapidement, la mémorisation de
l'URL n'est utile que momentanément. Plus intéressant est de vous
pouvez aussi mémoriser des <a
href="/comment_chercher_une_maison/"> requêtes vers des
agrégateurs</a> comme par exemple:

```
https://www.nestoria.fr/erquy/immobilier/vente
https://www.seloger.com/list.htm?&idtypebien=2&idtt=2,5&naturebien=1,2,4&ci=140117
```

La première URL affiche l'immobilier en vente à Erquy. La seconde
affiche les maisons à vendre à Cabourg. Ainsi vous pouvez rapidement
vérifier si de nouveaux biens apparaissent ou disparaissent!

## Analyser une annonce

Une nouvelle fonctionnalité est d'analyser une annonce pour en
extraire les principales caractéristiques (prix, surface, description,
photos, etc.). Ces informations sont stockées dans votre navigateur et
sont donc disponibles même si l'annonce disparait et même si vous êtes
hors connexion.

Attention, l'IA qui analyse les annonces n'est pas encore au summum de
ses capacités, elle sait couramment analyser les annonces des
principaux sites comme BellesDemeures, BienIci, ImmobilierFigaro,
ImmobilierNotaires, LeBonCoin, LogicImmo, Orpi, PAP, SeLoger,
SuperImmo et quelques autres.

Si les résultats de l'analyse sont incomplets voire faux, pas de
panique, vous pouvez les compléter et les modifier. Vous pouvez, par
exemple, copier-coller la description de l'annonce dans le champ
description de la fiche. Vous pouvez également rectifier les surfaces,
prix et autres nombres caractérisant le bien.

Personnellement, une fois l'annonce rectifiée, je ne la modifie plus.
En revanche, j'accumule dans la fiche les nouvelles données
(changement de prix, nouvelles informations obtenues après entretien
téléphonique, etc.). 

### Pourquoi ?

- Pourquoi plusieurs fois la même photo ? Parce que souvent elle
apparaît en plusieurs tailles dans l'annonce.

- Pourquoi des images blanches ? Parce que les sites d'annonces
insèrent souvent des images transparentes de 1 pixel sur 1 pixel sont
souvent utilisées (dans les pages, dans les courriels, etc.) afin de
savoir si les pages sont consultées.

- Pourquoi l'analyse n'a pas réussi ? Il y a de nombreuses possibilités:

-- la page que vous souhaitiez analyser contient plusieurs annonces
-- le serveur demande de résoudre une captcha
-- le format de l'annonce a changé

Mais <a href="{$docbase}/comment_installer_l_extension_trinvext/"> 
tout n'est pas perdu</a>, vous pouvez
installer l'extension TrinvExt!

## Actions sur fiche

Vous pouvez suivre l'URL de la fiche ou supprimer la fiche. Vous
pouvez ajouter des photos ou l'annoter pour, par exemple:
- noter un rendez-vous,
- noter un numéro de téléphone,
- noter vos impressions après une visite,
- passer en revue les photos de toutes les annonces associées
- gérer les photos (en ajouter ou en supprimer)
- etc.

### Ajouter une photo

Vous pouvez ajouter une photo depuis votre galerie de photo ou à
l'aide de l'appareil photo de votre dispositif si disponible. Cliquer
sur une photo l'affiche en grand et propose un menu permettant de
supprimer la photo ou de la choisir comme icône représentative de la
fiche.

### Supprimer une photo

Cliquer sur une photo permet de la voir en grand. À droite de la
photo, plusieurs boutons permettent de la fermer, de la choisir comme
icône de la fiche ou de la supprimer. 

### Supprimer des photos

On peut aussi cliquer sur le petit cercle situé en haut et à gauche
d'une photo et ainsi la sélectionner (on peut aussi en sélectionner
d'autres), cliquer sur la poubelle qui apparaît à la fin des photos
permet de supprimer, d'un seul coup, toutes les photos sélectionnées.

### Étiqueter la fiche

Une fois choisie une photo représentative de la fiche, cliquer dessus
permet d'étiqueter la fiche. Un menu s'ouvre permettant de choisir ce
que vous pensez faire ou avez fait avec le bien en question
(intéressant, se renseigner, vouloir visiter, visité), l'état du bien
(sous offre, sous compromis, vendu, disparu, réapparu) et, enfin,
l'intérêt que vous portez à ce bien de `---` à `+++`. 

<img itemprop="image" alt="Étiqueter une annonce" 
     class='inDocumentation'
     src="/Screens/interesting.webp" />

Enregistrez vos étiquettes, elles apparaîtront en bandeau sur la
vignette représentative de la fiche. Dans la liste des fiches, cliquer
sur la colonne des vignettes, les présente triées par intérêt.

<img itemprop="image" alt="Lister les annonces" 
     class='inDocumentation'
     src="/Screens/interestingRecords.webp" />


### Annoter une fiche

Vos annotations utilisent les conventions MarkDown. Si ce nom ne 
vous dit rien, vous pouvez ignorer ce que je viens d'écrire!

Si vous souhaitez employer du gras, de l'italique, des paragraphes, des
énumérations ou des liens, regardez l'exemple suivant:

<pre class='grey'>
Un *paragraphe* pas très long 
mais **sur plusieurs** lignes 
avec un mot en **gras** 
et un autre en *italiques* 

- une liste en
- trois points
- dont une [url vers TRINV](https://trinv.fr)
</pre>

ce qui donne:

<div class='grey'>

Un *paragraphe* pas très long 
mais **sur plusieurs** lignes 
avec un mot en **gras** 
et un autre en *italiques* 

- une liste en
- trois points
- dont une [url vers TRINV](https://trinv.fr)

</div>




