---
layout: pagelayout.liquid
pageTitle: Comment trouver une adresse
tags: [ 'Articles' ]
date: "2022-03-03"
---

<div itemscope itemtype='http://schema.org/HowTo'>
<div itemprop="description" 
     value="Comment trouver une adresse" ></div>
<div itemprop="author" itemscope itemtype="https://schema.org/Person">
  <div itemprop="name" value="Christian Queinnec" ></div>
</div>
<!-- div itemprop="supply" value="" / -->
<div itemprop="tool" value="Internet" ></div>
<div itemprop="totalTime" value="P1DT" />
<div itemprop="datePublished" value="{{ date }}" ></div>

Comment trouver l'adresse d'un bien vous intéressant ? Comment savoir
où se trouve le terrain ou la maison décrit dans l'annonce immobilière
? Hélas, l'adresse n'est pas indiquée sur l'annonce et vous souhaitez
légitimement inspecter le voisinage et les environs afin de décider si
vous allez visiter ou pas ce bien.

En effet, la présence d'un camping bruyant, d'une ferme porcine au
vent, d'une route importante sont des éléments importants à connaître
avant d'envisager une visite pouvant nécessiter un déplacement
lointain.

<!-- ************************** step **************** -->
<div itemscope itemprop="step" itemtype="http://schema.org/HowToStep">
<h2 itemprop="name">Zone</h2>
<div itemprop="text">

Très peu d'annonces donnent l'adresse du bien. Certaines indiquent une
zone de 500 mètres de rayon (mais souvent fausse), d'autres encore
donnent des renseignements tels que

- à 200 mètres de la mer
- à 1 encablure de la plage de X
- dans le quartier de Y
- à 10 minutes du golf 
- à proximité des commerces

Ce n'est pas simple à interpréter mais cela donne une idée de la zone
où peut se trouver la maison de l'annonce.

</div>
</div>

<!-- ************************** step **************** -->
<div itemscope itemprop="step" itemtype="http://schema.org/HowToStep">
<h2 itemprop="name">Cadastre inversé</h2>
<div itemprop="text">

Si d'aventure, la surface du terrain est indiquée, vous pouvez
essayer, grâce à TRINV, de
<a href="{$docbase}/comment_chercher_des_parcelles_cadastrales/">
trouver toutes les parcelles ayant cette surface</a>.
Dans la carte qui s'affiche, vous pouvez retirer toutes les parcelles
ne correspondant pas à la zone que vous avez pu établir à l'étape
précédente.

Plusieurs problèmes sont possibles:

- la surface indiquée dans l'annonce est fausse;
- le terrain est composé de plusieurs parcelles et l'application TRINV ne sait trouver qu'une unique parcelle;
- le terrain est en copropriété et ne constitue pas une parcelle cadastrale autonome;
- le cadastre n'est pas à jour car il n'est mis à jour que tous les trois mois.

</div>
</div>

<!-- ************************** step **************** -->
<div itemscope itemprop="step" itemtype="http://schema.org/HowToStep">
<h2 itemprop="name">Éléments caractéristiques</h2>
<div itemprop="text">

Regardez bien les photos de la maison dans toutes les annonces qui la
proposent afin de déterminer ses éléments caractéristiques:

- le nombre d'étages;
- la forme du toit (ou des toits);
- la présence de cheminées, d'antennes;
- la forme des fenêtres (vasistas, chien-assis, balcon, etc.);
- les terrasses, parkings, abris de jardin;
- les haies, buissons ou arbres;
- etc.

Certains éléments ne sont utiles que lors de l'analyse des photos
aériennes (forme des toits, cheminées, chien assis, terrasse, abri de
jardin, arbres, haies). D'autres ne sont utiles qu'avec StreetView:
cheminées, antennes, forme des fenêtres, portail, haies, arbres).

</div>
</div>

<!-- ************************** step **************** -->
<div itemscope itemprop="step" itemtype="http://schema.org/HowToStep">
<h2 itemprop="name">Le cas des piscines</h2>
<div itemprop="text">

Si d'aventure le bien que vous recherchez comporte une piscine, alors
cherchez plutôt avec le [cadastre](https://www.cadastre.gouv.fr/) car
les piscines y apparaissent en bleu. Les parcelles avec piscine sont
donc facilement localisables. Malheureusement, il y a de nombreuses
piscines non déclarées que le cadastre ne connaît donc pas!

</div>
</div>

<!-- ************************** step **************** -->
<div itemscope itemprop="step" itemtype="http://schema.org/HowToStep">
<h2 itemprop="name">StreetView</h2>
<div itemprop="text">

Il ne reste plus que quelques parcelles possibles avec la bonne
surface, comment identifier la bonne ? 

Si les voies passant devant les parcelles sont dans StreetView,
vous pouvez vérifier si les éléments caractéristiques apparaissent ou
pas. Autrement, il vous faut utiliser les cartes aériennes. 

</div>
</div>

<!-- ************************** step **************** -->
<div itemscope itemprop="step" itemtype="http://schema.org/HowToStep">
<h2 itemprop="name">Cartes aériennes</h2>
<div itemprop="text">

Tentez de dessiner la forme de la maison vue du ciel et indiquez où se
trouvent les éléments caractéristiques que vous avez pu identifier
(vasistas, chien assis, cheminée, toits multiples, terrasse, arbre,
allée carossable, etc.). Puis passez en revue toutes les maisons de la
zone où peut se trouver la maison recherchée.

Certaines formes de maison sont faciles à identifier.



</div>
</div>

</div>

