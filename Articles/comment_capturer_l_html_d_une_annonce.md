---
layout: pagelayout.liquid
pageTitle: Comment capturer l'HTML d'une annonce
tags: [ 'Articles' ]
date: "2024-02-29"
---

<div itemscope itemtype='http://schema.org/HowTo'>
<div itemprop="description" 
     value="Comment capturer l'annonce" ></div>
<div itemprop="author" itemscope itemtype="https://schema.org/Person">
  <div itemprop="name" value="Christian Queinnec" ></div>
</div>
<!-- div itemprop="supply" value="" / -->
<div itemprop="tool" value="Internet access" >&nbsp;</div>
<div itemprop="totalTime" value="P1M" >&nbsp;</div>
<div itemprop="tool" itemscope itemtype="https://schema.org/HowToTool">
    <div itemprop='url' value="https://trinv.fr/" >&nbsp;</div>
</div>
<div itemprop="datePublished" value="{{ date }}" ></div>

Dans le rare cas où les analyses automatiques n'ont pu extraire les
caractéristiques d'une annonce, il est possible d'utiliser la
procédure suivante ci-dessous détaillée.

Voici donc une annonce intéressante:

<img itemprop="image" alt="exemple d'annonce" 
     class='inDocumentation'
     src="/Screens/capture1.png" />

on clique dans cette page avec le bouton droit de la souris et on
sélectionne « inspecter »:

<img itemprop="image" alt="inspecter" 
     class='inDocumentation'
     src="/Screens/capture2.png" />
     
S'affiche alors dans le mode développeur, le code HTML de la page.
L'élément inspecté est en bleu.

<img itemprop="image" alt="HTML de la page" 
     class='inDocumentation'
     src="/Screens/capture3.png" />

Remonter puis cliquer, en haut, sur le premier élément: la balise
`<html>` et, enfin, sélectionner « éditer en HTML »

<img itemprop="image" alt="copier le code de la page entière" 
     class='inDocumentation'
     src="/Screens/capture4.png" />
     
Sélectionner la page entière et copier la (souvent CTRL-A puis CTRL-C)

<img itemprop="image" alt="copier la page" 
     class='inDocumentation'
     src="/Screens/capture5.png" />
     
Revenir au formulaire de TRINV:

<img itemprop="image" alt="inspecter" 
     class='inDocumentation'
     src="/Screens/capture6.png" />

Copier (souvent CTRL-V) le code dans le formulaire puis demander
l'analyse:

<img itemprop="image" alt="inspecter" 
     class='inDocumentation'
     src="/Screens/capture7.png" />

S'affichera alors, quelque temps après, la fiche synthétisée:

Et voila!
