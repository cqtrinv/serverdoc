---
layout: pagelayout.liquid
pageTitle: Comment gérer ses annonces
tags: [ 'Articles' ]
date: "2022-03-04"
---

## Pourquoi gérer les annonces ?

J'ai trouvé utile de conserver toutes les annonces que je recevais ou
que je consultais au fur et à mesure de leur apparition. Certaines
sont longtemps affichées (et donc non vendues ce qui laisse penser
qu'il y a un problème (de prix ou autre)), certaines refont surface
après quelques mois (au même prix ou pas) enfin, quelques autres
apparaissent dans d'autres agences avec des descriptions différentes.

Enfin, si votre recherche dure longtemps, vous aurez ainsi une idée
statistique et temporelle des biens en vente, des évolutions de prix,
des meilleurs mois où acheter, du temps moyen de mise sur le marché,
du style des agences ou de leur territoire favori, etc.

## Comment gérer les annonces ?

Lorsqu'une annonce est affichée dans votre navigateur, vous pouvez la
partager en indiquant l'application TRINV mais pour cela, vous devez
avoir préalablement
<a href="{$docbase}/comment_installer_trinv/">
installé TRINV comme application</a>.

Vous pouvez également fournir l'URL à TRINV pour mémorisation.

## Comment améliorer une annonce ?

Les annonces sont mémorisées dans une fiche sur laquelle quelques
actions sont possibles. Vous pouvez modifier le titre afin de
remplacer l'insipide URL par un libellé plus parlant (maison aux
volets bleus, ou maison visitée le 2 mars 2022, ou ...). Vous pouvez
également ajouter des commentaires comme par exemple (RV pris pour le
3 mars) ou des photos. Vous pouvez choisir la photo illustrant la
fiche, etc. Plus de détails en
<a href="{$docbase}/que_faire_avec_une_fiche/">
que faire avec une fiche</a>.

Rappelons-le, tout ceci s'effectue anonymement et seulement dans votre
navigateur. Si vous utilisez plusieurs machines et souhaitez voir ces
informations partout, les partager avec d'autres personnes, vous
devrez attendre de plus amples développements... 

## Construire son tableau de fiches

Sur écran large (de plus de 1000 pixels), les fiches sont affichées
avec leurs caractéristiques. 

<img itemprop="image" alt="fiches sur écran large"
     class='inDocumentation'
     src="/Screens/fullrecords.png" />
     
Cliquer sur un titre de colonne permet d'agir sur les fiches. Il est 
possible de trier (de façon ascendante ou descendante):

<img itemprop="image" alt="tri de fiches sur écran large"
     class='inDocumentation'
     src="/Screens/sortrecords.png" />
     
Il est possible aussi de filtrer les fiches par valeur:

<img itemprop="image" alt="filtrage de fiches sur écran large"
     class='inDocumentation'
     src="/Screens/filterrecords.png" />
    
mais aussi de rechercher un mot:

<img itemprop="image" alt="recherche de fiches sur écran large"
     class='inDocumentation'
     src="/Screens/searchrecords.png" />
     
ou encore de masquer des colonnes (ou de les faire réapparaître)
 
<img itemprop="image" alt="masquage de colonnes sur écran large"
     class='inDocumentation'
     src="/Screens/maskrecords.png" />

## Voir ses fiches autrement

Le menu local associé aux fiches (les 3 points verticaux à côté du
titre « Mes annonces ») offre plusieurs choix.

### Voir ses fiches sur une carte

L'entrée « Carte des fiches » affiche une carte indiquant la
localisation des fiches dont on connaît les coordonnées. 

### Voir ses fiches dans un CSV

L'entrée « CSV des fiches » engendre un fichier textuel (en termes plus
techniques: un CSV) que vous pouvez importer dans un tableur. 

## Et plus tard ?

Bientôt, vous pourrez étiqueter vos annonces pour mieux les classer
ou les ranger dans des dossiers.



