---
layout: indexpagelayout.liquid
pageTitle: Table des matières
article: 0.0
---

<p> 
    Vous trouverez, dans cette partie du site de TRINV, des
    informations sur l'application TRINV et sur 
    mes techniques de recherche de maisons.
</p>

<div class='w3-container'>
 <ul class='w3-ul w3-hoverable'>
  <li><button on:click={go}>
    <a href='{$hostbase}/about'>À propos de TRINV</a></button>
  </li>
{%- for post in collections.AllArticles -%}
  <li><button on:click={go} >
    <a href='{{ post.page.canonicalURL }}'>{{ post.data.pageTitle }}</a></button>
  </li>
{%- endfor -%}
  <li><button on:click={go} >
    <a href='{$hostbase}/cookies'>À propos des cookies</a></button>
  </li>
 </ul>
</div>

<style>
li a {
  text-decoration: none;
}
li button {
   min-width: 100%;
   text-align: left;
   background-color: transparent;
   border: unset;
}
</style>
