---
layout: pagelayout.liquid
pageTitle: Quoi de neuf dans TRINV
tags: [ 'Articles' ]
date: "2022-03-20"
---

Les nouveautés sont affichées en ordre anti-chronologique.

## 15 janvier 2025

Nouvelle moûture majeure de TRINV! Des myriades de petites
modifications, de légères améliorations et de substantielles révisions
internes de programmation. 

La grande nouveauté, disponible pour tous, est l'accès aux DPE
(Diagnostic de Performance Énergétique) facilitant la recherche des
adresses de biens. Et, puisque le Finistere est le département le plus
recherché dans TRINV, voici, par exemple, les 
<a href='{$hostbase}/carte-dpe?inseeid=29225'>DPE de Pouldreuzic</a>.

<img class='inDocumentation'
     alt="DPE de Pouldreuzic"
     src='{$docbase}/dpe-pouldreuzic.png'>

<a href='{$docbase}/a_propos_des_dpes/'>Pour en savoir plus</a>

## 24 août 2024

Les limites des parcelles cadastrales sont maintenant affichables.
Sur une carte, le bouton M permet de passer de la carte à la vue
des parcelles puis à la vue satellitaire.

## 20 août 2024

Deux grands changements:

Refonte importante de l'analyse de résultats d'aggrégateurs
(MoteurImmo, SeLoger, Nestoria) et rédaction de la documentation
afférente. L'essayer c'est l'adopter mais cela requiert 
l'<a href='{$docbase}/comment_installer_l_extension_trinvext/'>extension TRINVext</a>.

Limitation du nombre de boutons apparents lors de la modification
d'une fiche: cliquez sur les caractéristiques suffit!

## 2 août 2024

Changé plein de petits détails. 

## 4 juin 2024

La synchronisation de fiches avec le serveur de TRINV permet aux
détenteurs de super-pouvoirs de partager leurs fiches sur plusieurs
appareils. Vous les modifiez sur un appareil et les retrouvez sur
un autre.

## 20 mai 2024

Il est possible, avec l'extension TRINVext, d'analyser certaines pages
d'aggrégateurs listant de nombreuses annonces ce qui permet de
surveiller les annonces d'un site aggrégateur, et ainsi

- visualiser les annonces nouvelles et les mémoriser en des fiches
- ignorer certaines annonces car inintéressantes
- ou bien chercher si l'annonce n'est que la réédition d'une annonce
  que vous connaissiez déjà!

## 17 mai 2024

La fusion de fiches peut maintenant s'effectuer par glisser-déposer.
À partir de la page affichant toutes les fiches, appuyer sur une fiche
pour la sélectionner puis faites la glisser sur la fiche avec laquelle
vous souhaitez la fusionner. Vous aurez ensuite à accepter ou pas, la
fusion proposée.

## 18 février 2024

Dans une fiche, cliquer sur la vignette permet de l'étiqueter. Les
étiquettes apparaissent alors sur la vignette et renseignent sur son
statut. Pratique pour visualiser les fiches intéressantes.

## 16 décembre 2023

Le choix d'une commune affiche la carte de celle-ci. La nouveauté est
que l'on peut interagir avec cette carte en cliquant dessus afin de
faire apparaître la parcelle, sa surface, ses coordonnées
géographiques ainsi que possiblement son adresse. Bien sûr les boutons
menant au x vues cadastrale ou satellitaire sont toujours présents. 

Cette nouveauté permet de se renseigner sur un endroit précis dont on
connait la localisation sans pour autant connaître sa surface.

La nouvelle version, aujourd'hui déployée, introduit deux sondages
anonymes, auxquels vous ne pourrez répondre qu'une seule fois,
permettant de déterminer d'éventuels futurs développements.

## 2 octobre 2023

Suite à une très intéressante suggestion, la liste des fiches
mémorise maintenant non seulement la liste des colonnes apparaissant
mais aussi le dernier tri effectué. Vous pouvez donc rafraîchir ou
recharger la page et retrouver l'ordre de vos fiches.

Par ailleurs, comme beaucoup ignoraient qu'ils pouvaient indiquer une
plage de surface min-max, une petite aide apparaît désormais pour leur
rappeler cette possibilité (dont, rappelons-le, il ne faut pas abuser).

## 30 août 2023

Devant les difficultés rencontrées lors de l'analyse des annonces
immobilières, TRINV préconise maintenant le seul emploi de l'extension
TRINVext. Cette solution est largement plus efficace que les solutions
précédentes et semble de plus en plus adoptée par les utilisateurs de
TRINV. 

## 12 avril 2023

Cette nouvelle version repose sur une importante modification de la
base de données mais cette modification est largement invisible des
utilisateurs. 

Par ailleurs, il est possible d'afficher sur une même carte toutes les
annonces immobilières que vous avez mémorisées et géo-localisées. Vous
pouvez également en extraire un fichier CSV que vous pourrez manipuler
via un tableur. 

## 29 mars 2023

Outre de nombreuses petites améliorations par-ci, par-là, la nouvelle
version de TRINV introduit une proposition de participation à
l'entrainement de l'IA d'analyse d'annonces immobilières. En effet,
comme pour de nombreuses méthodes d'entrainement, il faut une
multitude de données! Vous pouvez bien sûr décliner.

## 16 janvier 2023

Une extension à Chrome, Brave, Edge, Opera est désormais disponible dans le
[magasin des extensions](https://chrome.google.com/webstore/detail/trinvext/kdbfegefefgjkifgedmmdmdddkglijgh). Elle est aussi [disponible pour Firefox](https://addons.mozilla.org/en-US/firefox/addon/trinvext/)
Cliquer sur cette extension lorsque vous êtes sur la page d'une
annonce immobilière, l'envoie directement à TRINV pour analyse puis
mémorise le résultat dans votre navigateur.
<a href="{$docbase}/comment_installer_l_extension_trinvext/">
Pour plus de détails</a>!

<div class='photos'>
 <img class='inDocumentation'
      alt="Emploi de l'extension TrinvExt - étape 1"
      src='/Screens/trinvextuse1.png'>
 <img class='inDocumentation'
      alt="Emploi de l'extension TrinvExt - étape 2"
      src='/Screens/trinvextuse2.png'>
 <img class='inDocumentation'
      alt="Emploi de l'extension TrinvExt - étape 3"
      src='/Screens/trinvextuse3.png'>
</div>


## 8 janvier 2023

Si vous avez la chance d'avoir un grand écran (de plus de 1000 pixels
de large), l'affichage de vos fiches montrent toutes leurs principales
caractéristiques. L'affichage est plus sommaire sur des écrans moins
larges voire même petits:

<div class='imgs'>
 <img itemprop="image" alt="fiches sur écran large"
     class='inDocumentation'
     src="/Screens/fullrecords.png" />
 <img itemprop="image" alt="fiches sur écran moyen"
     class='inDocumentation'
     src="/Screens/defaultrecords.png" />
 <img itemprop="image" alt="fiches sur écran petit"
     class='inDocumentation'
     src="/Screens/smallrecords.png" />
</div>

Cliquer sur un titre permet de trier, filtrer ou masquer les
fiches. Vous pouvez ainsi vous construire le tableau qui vous sied. 

## 22 décembre 2022

Une nouvelle et bien meilleure version de l'IA d'analyse d'annonces
immobilières vient d'être déployée. Malheureusement il y a encore des
sites récalcitrants mais la fonctionnalité de ré-analyse permet de
raffiner les informations extraites.

Au fait, vous êtes plus de 30 par jour à interroger TRINV et vos
recherches ont porté sur plus de 2500 communes en France. Malgré cela,
les ressources dues à la publicité ne rapportent qu'environ 1 EURO par
mois ce qui ne suffit pas à assurer le coût des machines. 

## 6 décembre 2022

Et, pour préparer Noël, encore une nouvelle version de TRINV! Celle-ci
permet maintenant de mieux fusionner des fiches, de supprimer des photos
en masse et de ré-analyser les annonces récalcitrantes! 

Vous suspectez un bien d'être dans un certain coin ? Cliquez dessus et
visualisez le cadastre (ou une vue satellitaire) de cet endroit

<img itemprop="image" alt="autres vues détaillées"
     class='inDocumentation'
     src="/Screens/popup2.png" />

## 23 novembre 2022

Nouvelles améliorations concernant les fiches et la gestion des photos
associées. Il est désormais possible de supprimer des photos en masse, 
de passer les photos en revue d'un seul clic. Il est également possible
de fusionner des fiches lorsqu'elles concernent le même bien: on a alors
plusieurs annonces réunies dans une même fiche.

## 24 octobre 2022

Nouvelles améliorations, les fiches mémorisées ainsi que les annonces
immobilières collectées peuvent maintenant être modifiées ou enrichies.

<img alt="fiches"
     class='inDocumentation'
     src="/Screens/fiches.jpg" />

## 29 septembre 2022

Nouvelle version de TRINV améliorant la recherche de parcelles. Les
cartes peuvent être affichées géographiquement ou en vue aérienne. On
peut aussi restreindre la recherche de parcelles sur une zone
particulière en éliminant toutes les parcelles hors de vue (après
avoir zoomé).

<img alt="map, zoom, crop" 
     class='inDocumentation'
     src="/Screens/msquare-annotated.svg" />

## 17 juin 2022

L'analyse d'annonces immobilières est désormais présente. À partir
d'une URL (que vous copiez à la main ou par partage), l'annonce est
scrutée, les principales caractéristiques sont extraites et
synthétisées en une fiche.

## 4 avril 2022

La nouvelle version introduit la notion de fiches liées à une URL.
On peut y stocker des annonces immobilières, ses annotations et des photos. 
<a href="{$docbase}/comment_gerer_ses_annonces/">
Pour en savoir plus</a>

## 25 janvier 2022

Les communes ayant juste changé de nom par suppression de tirets ne
sont plus mentionnées avec leur ancien nom. 

Encore quelques centaines de communes ont disparu ces derniers temps
et sont maintenant mieux affichées (Tocqueville-sur-Eu, par exemple).

## 26 décembre 2021

Un nouveau bouton 'Cadastre' permet de voir la forme et les limites de
la parcelle cadastrale recherchée. Le bouton 'Voir' affiche la vue
aérienne via Google afin de permettre d'utiliser StreetView lorsque
possible.

## 23 novembre 2021

Il est possible de supprimer certaines communes de la liste des communes
les plus souvent demandées.

## 13 novembre 2021

Les communes les plus souvent demandées sont maintenant proposées
sur la page de choix de la commune à investiguer.

## 12 novembre 2021

Lors de l'affichage de la carte avec les parcelles recherchées, les
listes sont maintenant réactives: cliquer sur une adresse révèle le
popup associé. 

Lorsque TRINV est utilisé sur un petit écran, cliquer sur le mot
`Adresse` dans la liste en bas de page permet de remonter les adresses
non nécessairement visibles.




