---
layout: pagelayout.liquid
pageTitle: Comment chercher une maison
tags: [ 'Articles' ]
date: "2022-03-01"
---

<div itemscope itemtype='http://schema.org/HowTo'>
<div itemprop="description" 
     value="Comment chercher une maison une maison à acheter" ></div>
<div itemprop="author" itemscope itemtype="https://schema.org/Person">
  <div itemprop="name" value="Christian Queinnec" ></div>
</div>
<!-- div itemprop="supply" value="" / -->
<div itemprop="tool" value="Internet access" ></div>
<div itemprop="totalTime" value="P1YT" />
<div itemprop="tool" itemscope itemtype="https://schema.org/HowToTool">
    <div itemprop='url' value="https://trinv.fr/" ></div>
</div>
<div itemprop="datePublished" value="{{ date }}" ></div>

Comment chercher une maison à acheter ? Cette page contient quelques
conseils pour vos propres recherches. La recherche d'un bien est un
exercice pouvant prendre de quelques jours à quelques années.

<!-- ************************** step **************** -->
<div itemscope itemprop="step" itemtype="http://schema.org/HowToStep">
<h2 itemprop="name">Zone</h2>
<div itemprop="text">

Définissez dans quelle zone (dans quelles communes) vous cherchez une
maison ou un terrain ? Attention, plus la zone est large et plus vous
aurez de chances de trouver le bien idéal mais, en revanche, plus vous
aurez de mal à suivre toutes les annonces qui s'y rapportent.

</div>
</div>

<!-- ************************** step **************** -->
<div itemscope itemprop="step" itemtype="http://schema.org/HowToStep">
<h2 itemprop="name">Sources d'information</h2>
<div itemprop="text">

Une fois la zone définie, il vous faut déterminer les agences y
proposant des biens. Les agences, ainsi que les notaires, ne
s'occupent souvent que de quelques communes et il n'y a souvent qu'une
ou deux agences principales.

À côté des agences existent aussi les aggrégateurs d'annonces tels que
[seLoger](https://www.seloger.com/) ou
[BienIci](https://www.bienici.com/) ou
[FigaroImmo](https://immobilier.lefigaro.fr/annonces/) 
et bien d'autres.

Enfin existent les méta-aggrégateurs tels que
[Nestoria](https://www.nestoria.fr/) ou
[MoteurImmo](https://moteurimmo.fr/) qui recensent les annonces des
agences et des aggrégateurs.

**Astuce**: afin de vous simplifier la vie, vous pouvez 
<a href="{$docbase}/memoriser_une_requete/">
mémoriser les requêtes</a>
à ces sites.

</div>
</div>

<!-- ************************** step **************** -->
<div itemscope itemprop="step" itemtype="http://schema.org/HowToStep">
<h2 itemprop="name">Abonnement</h2>
<div itemprop="text">

Pour toutes ces sources d'informations, n'hésitez pas à vous abonner
afin de recevoir des alertes lorsque de nouveaux biens apparaissent.
Il ne faut en effet pas perdre trop de temps car certaines annonces ne
paraissent que quelques jours.

Mon conseil est d'avoir des critères assez larges pour plusieurs
raisons.

- Le nombre de chambres ou de pièces d'une maison varie car les
  agences ne les décomptent pas de la même manière.
- La surface des maisons est souvent approximative tant que les
  mesures Carrez ne sont pas effectuées.
- Les prix varient en fonction des agences et de leurs frais, ils
  varient aussi au fil du temps surtout lorsque le bien ne se vend pas
  et que les vendeurs diminuent leur prix.

Personnellement, je n'indique pas le nombre minimal de chambres ou de
pièces car suivant le plan de la maison (cloisons ou murs porteurs) ce
sont des éléments qui peuvent être modifiés. De plus de nombreuses
maisons peuvent être étendues (selon les PLU (Plan Local
d'Urbanisme)). En revanche, j'indique la surface habitable minimale
souhaitée (avec une marge basse) ainsi que la taille minimale de la
parcelle (encore une fois avec une marge basse).

</div>
</div>

<!-- ************************** step **************** -->
<div itemscope itemprop="step" itemtype="http://schema.org/HowToStep">
<h2 itemprop="name">Critères</h2>
<div itemprop="text">

Vous recevez maintenant plein d'annonces, comment les interpréter ?

Définissez vos critères, ce qui vous importe, ce qui vous indiffère,
ce que vous voulez savoir sans avoir d'avis préconçu, ce que vous ne
pouvez accepter. Voici par exemple quelques critères:

- emplacement, voisinage, bruit, mitoyenneté;
- gamme de prix;
- surface habitable;
- surface terrain;
- habitable (avec rafraîchissement);
- proximité de route passante;
- présence du tout-à-l'égoût;
- distance plage, gare, commerces;
- vue mer, montagne;
- orientation;
- campagne, zone résidentielle, centre bourg;
- présence d'eau (étang, puits);
- servitudes;
- double vitrage;
- etc.

À côté de ces critères, demandez-vous aussi pourquoi vous voulez acheter 
un bien. Est-ce pour

- se reposer,
- recevoir des amis,
- garder ses enfants ou petits-enfants,
- faire des promenades,
- y vivre en hiver
- etc.

</div>
</div>

<!-- ************************** step **************** -->
<div itemscope itemprop="step" itemtype="http://schema.org/HowToStep">
<h2 itemprop="name">Annonces</h2>
<div itemprop="text">

Une annonce vous intéresse! Si elle provient d'un aggrégateur, je vous
conseille d'aller consulter l'annonce originale sur le site de
l'agence, elle est souvent plus étoffée, elle contient souvent plus de photos
et plus de renseignements.

Lorsque l'annonce n'est pas exclusive, elle est souvent proposée par
d'autres agences et parfois à des prix différents. Les photos peuvent
varier ainsi que les renseignements associés. <a
href="{$docbase}/memoriser_une_annonce_immobiliere/"> Mémorisez ces
annonces</a> et fusionnez-les afin de réunir en une même fiche toutes
les informations.

<div id="fausseNouveaute" />>
Les annonces peuvent évoluer et être alors affublée de la mention «
nouveau »: le prix a pu changer ou le bien a pu revenir sur le marché
(après une vente infructueuse). Dans beaucoup d'autres cas, l'annonce
n'a pas évolué et a juste été remise en avant sans être pour autant
nouvelle mais cela peut aussi être dû à une nouvelle agence mandatée
pour la vente du bien ou à un nouvel aggrégateur qui vient de
découvrir une annonce existante. Bref, l'étiquette « nouveau » peut ne
pas nécessairement signaler un nouveau bien.

</div>
</div>

<!-- ************************** step **************** -->
<div itemscope itemprop="step" itemtype="http://schema.org/HowToStep">
<h2 itemprop="name">En savoir plus</h2>
<div itemprop="text">

Un bien a retenu votre attention! Il ne reste plus qu'à en
savoir plus. 

- Tout d'abord, recensez toutes les annonces des différentes agences 
  offrant le même bien;
- Comparez les descriptifs et les photos;
- Beaucoup de maisons en bord de mer sont aussi proposées à la
  location saisonnière, scrutez les sites d'annonces
  ([AirBnB](https://airbnb.fr/), [Abritel](https://www.abritel.fr/),
  etc.) car souvent on y trouve l'adresse et beaucoup d'autres photos.

</div>
</div>

<!-- ************************** step **************** -->
<div itemscope itemprop="step" itemtype="http://schema.org/HowToStep">
<h2 itemprop="name">Gérer les annonces</h2>
<div itemprop="text">

Dans tous les cas, prenez note de toutes les annonces que vous avez
parcourues ou <a href="{$docbase}/comment_gerer_ses_annonces/">
mémorisez les</a>. 
Prenez des notes sur vos actions (téléphone, nom de l'interlocuteur,
renseignements, rendez-vous, résultat, etc.)

</div>
</div>

<!-- ************************** step **************** -->
<div itemscope itemprop="step" itemtype="http://schema.org/HowToStep">
<h2 itemprop="name">Localiser un bien</h2>
<div itemprop="text">

Ça y est, un bien a vraiment retenu votre attention! À l'aide de
toutes les photos accumulées et de tous les descriptifs:

- Essayez d'esquisser un plan de la maison et de sa parcelle
- Sélectionnez l'agence qui vous paraît la plus appropriée
- Téléphonez pour en savoir plus et notamment l'adresse
- Avec l'adresse, consultez les cartes géographiques et les vues
  aériennes afin d'apprécier la situation du bien.
  
Si vous n'obtenez pas l'adresse ou la zone où se trouve le bien, il ne
vous reste plus qu'à tenter de le localiser par vos propres moyens
mais surtout grâce à TRINV! Mais ceci est un 
<a href="{$docbase}/comment_trouver_une_adresse/">
vaste sujet développé dans un autre article</a>.

</div>
</div>

</div>
