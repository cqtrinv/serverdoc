---
layout: pagelayout.liquid
pageTitle: Règles de confidentialité
tags: [ 'Articles' ]
date: "2022-03-20"
---

## L'extension `TrinvExt`

L'utilisation de l'extension `TrinvExt` n'utilise aucune donnée
personnelle. L'utiliser envoie le contenu de la page courante vers le
site TRINV pour analyse, extraction des principales caractéristiques
et renvoi de ces données pour mémorisation dans le navigateur de
l'utilisateur.

## Le site TRINV

Le site [TRINV](https://trinv.fr) utilise des cookies.
[Pour en savoir plus sur ces cookies](https://trinv.fr/cookies). La
publicité qui apparaît en haut des pages est assurée par Google qui
donc dépose ses propres cookies pour ce faire.

TRINV enregistre les connexions (heure, numéro IP, requête) dans ses
journaux et les mémorise au plus un an. TRINV enregistre aussi
l'activité des utilisateurs avec un cookie de session, cookie qui peut
être associé (mais seulement si l'utilisateur s'est identifié auprès
de TRINV) à son adresse de courriel et à son pseudo.

Le pseudo pourrait, dans de possibles futurs développements,
apparaître lors de partages d'informations. 

Les connexions et activités permettent de comprendre comment se
comportent les utilisateurs de TRINV afin d'améliorer le site.
