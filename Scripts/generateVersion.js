// Generate a JSON file with the current version of TRINV server

import fs from 'fs';

const pkg = JSON.parse(fs.readFileSync('./package.json'));
const version = pkg.version;

const result = JSON.stringify({ version, alive: true }).toString();
fs.writeFileSync("./static/version.json", `${result}\n`);

// end
